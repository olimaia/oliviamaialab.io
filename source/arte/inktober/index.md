---
title: inktobers
---

## ink-quê?

[inktober](https://inktober.com/) é um *desafio* anual criado em 2009 pelo ilustrador Jake Parker. a ideia é fazer uma ilustração em nanquim (ou similar) por dia durante todo o mês de outubro.

minha primeira participação foi em 2017. controvérsias à parte, o importante é ter desculpa pra praticar e rabiscar. 

compartilho aqui os resultados:

- [![2020](/img/ink2020.png) **2020**](/arte/inktober/2020)
- [![2018](/img/ink2018.png) **2018**](/arte/inktober/2018)
- [![2017](/img/ink2017.png) **2017**](/arte/inktober/2017)
{.rabiscos-listagem}
