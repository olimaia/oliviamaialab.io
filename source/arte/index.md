---
title: arte
---

## rabiscologias

rabiscologia é a ciência dos rabiscos. é um espaço de estudo, experiências, testes e, principalmente, um espaço de liberdade: meu projeto artístico pessoal.

costumo fazer [_lives_ de ilustração](https://live.oliviamaia.net) e você pode [pedir uma arte original](/arte/originais), que mando via correios pelo valor mínimo dos custos de envio. vídeos em _timelapse_ estão disponíveis [em meu canal no peertube](https://share.tube/video-channels/art_things).

aqui você pode conhecer (e, no caso das ilustrações, *imprimir e botar na parede*) meu trabalho:

- [![rabiscos](/img/rabiscos-p.png) ilustração e rabiscos](/arte/rabiscos)
- [![originais](/img/rabiscos-originais.jpg) originais disponíveis](/arte/originais)
- [![inktobers](/img/ink-arte.png) inktobers](/arte/inktober)
{.rabiscos-listagem}
