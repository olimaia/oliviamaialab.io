---
title: f.a.q.
---

_ou: as perguntas que ninguém fez mas eu vou responder mesmo assim._

![ovos coloridos no supermercado suíço](ovosuico.jpg)
_por que as galinhas suíças botam ovos coloridos?_
{.center .short-line}

## que quer dizer isso de _copyleft_? o que eu posso fazer com o seu trabalho?

todo o site, incluindo as ilustrações da página de ilustrações e os zines, estão sob licença [creative commons por atribuição](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR). significa que pode fazer o que quiser, inclusive trabalhos derivados ou comerciais, _desde que liberados sob a mesma licença_, e desde que meu trabalho _seja creditado_. meu nome e um link pro meu site já basta, onde for mais conveniente (levando um conta um pouco de bom senso).

## posso ganhar muito dinheiro vendendo camisetas com suas ilustrações?

na teoria, sim — desde que meu trabalho seja devidamente creditado, desde que você não diga que o trabalho é seu, e desde que você compreenda que não tem nenhuma exclusividade de uso das ilustrações. na prática, eu espero bom senso; se sua intenção é ganhar dinheiro com minhas ilustrações, seria o caso de contribuir com alguma coisa no momento de fazer o download.

## pode tatuar?

pode — e claro que não precisa dar crédito na tatuagem. mas se meu trabalho é importante pra você a ponto de você querer uma tatuagem, eu agradeço muito se puder também [contribuir com ele](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=D3MSR4A6T4F2J&source=url), “comprando” a ilustração pelo valor que couber no seu bolso.

## mas pode mesmo baixar tudo de graça?

todas as ilustrações e zines estão disponíveis em formatos digitais para download gratuito — então: sim, pode. se puder e quiser [contribuir](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=D3MSR4A6T4F2J&source=url) com qualquer valor, eu agradeço. se não puder, fique à vontade pra fazer o download mesmo assim. se puder me mandar um e-mail ou mensagem em alguma rede social eu também agradeço :)

## como faz para baixar de graça?

na página de zines ou de ilustrações, basta clicar no link de download, seja do PDF ou da imagem em formato _.png_.

## e se eu quiser contribuir? qual seria um valor adequado?

não tem disso não. pode contribuir com quanto quiser: pouco, muito, mais ou menos. só depende de você.

## você fica ofendida se eu contribuir com um real?

não — pode contribuir com quanto quiser e puder.

## e se eu quiser uma versão impressa assinada?

é só pedir por e-mail ou mensagem. todas as impressões de zines e ilustrações são feitas em casa, artesanalmente e com amor.

## os livros vêm autografados?

a princípio sim. se quiser o autógrafo para outra pessoa, ou se preferir o livro sem rabisco nenhum, é só avisar.

## você faz ilustração por encomenda?

talvez — depende da encomenda. entra em contato e a gente conversa.

## de graça?

de graça eu faço minha arte e meus rabiscos; trabalho eu cobro porque afinal tem que pagar a ração dos muitos gatos que moram na minha casa.

## você não faz mais revisão de texto e leitura crítica de ficção?

faço, mas sou chata, rabugenta e cobro caro. não me recomendo. posso indicar amigos que fazem um ótimo trabalho por preços muito melhores.

---

_tem mais alguma pergunta? manda pra mim: [olivia@oliviamaia.net](mailto:olivia@oliviamaia.net) ou [@oliviamaia](https://www.t.me/oliviamaia) no telegram._
