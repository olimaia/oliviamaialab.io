---
title: originals
category: eng
photos:
	- /img/originais/capture000137.jpg
	- /img/originais/capture000134.jpg
	- /img/originais/capture000131.jpg
	- /img/originais/capture000130.jpg
	- /img/originais/capture000129.jpg
	- /img/originais/capture000127.jpg
	- /img/originais/capture000126.jpg
	- /img/originais/capture000120.jpg
	- /img/originais/capture000119.jpg
	- /img/originais/capture000113.jpg
	- /img/originais/capture000112.jpg
	- /img/originais/capture000111.jpg
	- /img/originais/capture000110.jpg
	- /img/originais/capture000104.jpg
	- /img/originais/capture000102.jpg
	- /img/originais/capture000100.jpg
	- /img/originais/capture000099.jpg
	- /img/originais/capture000096.jpg
	- /img/originais/capture000095.jpg
	- /img/originais/capture000094.jpg
	- /img/originais/capture000092.jpg
	- /img/originais/capture000091.jpg
	- /img/originais/capture000082.jpg
	- /img/originais/capture000076.jpg
	- /img/originais/capture000071.jpg
	- /img/originais/capture000066.jpg
	- /img/originais/capture000064.jpg
	- /img/originais/capture000063.jpg
	- /img/originais/capture000055.jpg
	- /img/originais/capture000052.jpg
	- /img/originais/capture000051.jpg
	- /img/originais/capture000049.jpg
	- /img/originais/capture000048.jpg
	- /img/originais/capture000046.jpg
	- /img/originais/capture000045.jpg
	- /img/originais/capture000042.jpg
	- /img/originais/capture000040.jpg
	- /img/originais/capture000038.jpg
	- /img/originais/capture000037.jpg
	- /img/originais/capture000036.jpg
	- /img/originais/capture000033.jpg
	- /img/originais/capture000032.jpg
	- /img/originais/capture000031.jpg
	- /img/originais/capture000026.jpg
	- /img/originais/capture000023.jpg
	- /img/originais/capture000004.jpg
	- /img/originais/capture000005.jpg
	- /img/originais/capture000006.jpg
	- /img/originais/capture000009.jpg
	- /img/originais/capture000010.jpg
	- /img/originais/capture000011.jpg
	- /img/originais/capture000012.jpg
	- /img/originais/capture000013.jpg
	- /img/originais/capture000017.jpg
	- /img/originais/capture000018.jpg
	- /img/originais/capture000019.jpg
	- /img/originais/capture000020.jpg
	- /img/originais/capture000021.jpg
	- /img/originais/capture000022.jpg
---

# claim a drawing!

these are my original drawings and art available for claiming.

price: **pay what you want**!
minimum price: shipping costs from Brazil (postage fee, envelope etc)

payment via **paypal**.

let's spread art!

to claim your art and for information regarding paper type/gramature, size, techniques and other enquiries, send me a message via e-mail (olivia@oliviamaia.net), [telegram](https://www.t.me/oliviamaia) or [mastodon](https://eldritch.cafe/@olivia).

you can also still download high resolution versions of [all my art](/en/art/) for free.
