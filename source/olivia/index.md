---
title: olivia quem?
---

![foto de perfil](/img/foto-perfil.jpg =300x){.right .foto-perfil} meu nome é Olivia Maia; sou artista, escritora e rabiscadora. nasci em São Paulo e, em agosto de 2013, deixei o trabalho como professora e fui embora de São Paulo com uma mochila nas costas. depois de dois anos e dois pares de botas entre os Andes e os Alpes, me instalei no interior da Bahia, na cidade de Lençóis.

## entre em contato

olivia@oliviamaia.net | [telegram](https://www.t.me/oliviamaia)

## me encontre na internet

- **mastodon_pt-br**: [masto.donte.com.br/@olivia](https://masto.donte.com.br/@olivia)
- **mastodon_en**: [eldritch.cafe/@olivia](https://eldritch.cafe/@olivia)
- **lives**: [live.oliviamaia.net](https://live.oliviamaia.net)
- **vídeos**: [share.tube/@olivia](https://share.tube/accounts/olivia)
- **fotos**: [fotos.oliviamaia.net](https://fotos.oliviamaia.net)

## literatura

existo na internet desde 2002 e comecei a publicar literatura em 2006. meu livro mais recente, [**TRÉGUA**](/livros/tregua), foi publicado via financiamento coletivo no catarse. [veja aqui](/livros) os romances e contos que já publiquei.

## arte & zine & rabiscos

os [zines](/zine) e [meu trabalho de ilustração](/arte) estão disponíveis como download gratuito do arquivo em alta resolução sob [licença creative commons CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR). bora espalhar arte.

você pode me ver fazer ilustrações e miniaturas ao vivo (em inglês) em [live.oliviamaia.net](https://live.oliviamaia.net). publico vídeos das _lives_ em timelapse (velocidade acelerada) no meu [canal de arte no peertube](https://share.tube/video-channels/art_things).

## apoie meu trabalho

- **catarse**: [catarse/oliviamaia](https://catarse.me/oliviamaia)  
- **liberapay**: [liberapay/nyex](https://pt.liberapay.com/nyex)  
- **paypal**: [página de doação única](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=D3MSR4A6T4F2J&source=url)  
