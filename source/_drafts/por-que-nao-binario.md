---
title: por que não-binário
categories: blog
tags:
  - gênero
  - pronomes
  - não-binário
date: 2020-02-14 17:29:55
---

eu sou gênero feminino como uma mesa, uma porta, uma árvore. porque são gênero feminino. por hábito, e principalmente porque eu gosto de gramática. não quer dizer que a porta é mais feminina do que o sofá. é uma formulação de linguagem.

meu centro de energia é feminino, no sentido yin-yang da coisa. 'feminino' aqui não tem nada a ver com gênero feminino; é uma convenção mais do que qualquer outra coisa, principalmente porque na maior parte das vezes são as mulheres que têm o centro de energia feminino (mas não sempre). energia feminina tem a ver com movimento e transformação. tem lados positivos e negativos, como tudo na vida. mas porque a gente vive numa sociedade que vê gênero de forma hierárquica, as coisas associadas à energia feminina são muitas vezes vistas como fúteis, desimportantes, histéricas. mas isso é a sociedade. eu não quero aceitar isso.

"quem não habita não tem hábitos," Gunther Anders escreveu no seu ensaio sobre Kafka. "e entende os costumes como decretos." dessa forma não me vejo como _mulher_, mesmo que gramaticalmente, digamos, eu não me importe com ser gênero feminino.

e da mesma forma não me vejo como _homem_, e isso talvez pelo tanto que me faz sentido essa energia feminina que me habita. por ser assexual eu não entendo a atração sexual, mesmo que eu entenda o que ela _deveria_ ser. então por ser uma pessoa não-binária eu não entendo o que significa _femininidade_, ou mesmo o que poderia significar _sentir-se mulher_. mas a etiqueta, o rótulo NÃO-BINÁRIO -- isso vem depois. primeiro é só isso: não habitar.

minha expressão de gênero é mais masculina. cabelo curto, roupa de moleque. isso não quer dizer que me sinta _homem_. não sou. homem é um bicho esquisito. dá pra dizer que entendo melhor as mulheres, mas isso deve ter muito mais a vez com a questão energética do que com _ser mulher_. mulheres, também, costumam entender melhor o tanto que a sociedade impõe padrões e obrigações que muitas vezes não fazem nenhum sentido.

tenho um amigo homem que se identifica como _queer_. ele é basicamente hetero e cis, mas pra ele se identificar como queer é uma forma de dizer que não quer se conformar com as imposições da sociedade, com seu próprio privilégio. uma forma de dizer que ele não pertence àquele mundo e que ele não vai ser o _homem_ que o mundo espera que ele seja. claro que ele têm privilégios por ser homem. ele sabe disso. mas essa é sua forma de estar consciente disso, de tentar viver contra isso.

é o tipo de pessoa com quem eu costumo me dar bem.

apesar da minha apresentação mais masculina, claro que a sociedade (e você, talvez) ainda me vê como mulher. eu sou casada com um homem. ele não é bem a coisa mais hetero do mundo e tampouco segue o manual binário de comportamento, mas ele é cis suficiente. a gente é um casal secretamente queer.

(era. agora todo mundo já sabe a verdade.)

e claro que ser _mulher_ nessa sociedade traz problemas. sempre peço ao marido pra lidar com os caras da internet, por exemplo, porque comigo é sempre _já tentou desligar e ligar de novo?_ e esse tipo de coisa. mas, enfim, essa é a sociedade. pra além disso, não me importo de ser vista como uma mulher, ainda que uma mulher que parece mei lésbica (ou o que a sociedade espera de uma lésbica), porque pra mim não faz muita diferença. eu sou isso: uma Olivia, o que deve ser um tipo de pessoa. pode chamar do que quiser. pra mim, _ser mulher_ não significa nada pra além da gramática.

meu problema é outro: a sociedade. não a linguagem. meu idioma é minha pátria. eu preferia uma sociedade que não vê diferença entre _homem_ e _mulher_. uma sociedade em qeu você pode ser um ou o outro e pronto, ninguém se incomoda, ninguém olha feio, ninguém acha que você sabe menos matemática. em que ser um ou outro tampouco tem muito a ver com a roupa que você usa ou seu sistema reprodutivo. porque tem gente que prefere a cor verde, foda-se. uma sociedade em que você é o que você é, e os outros não têm nada a ver com isso (principalmente os outros que não gostam de você). mas também uma sociedade em que qualidades 'femininas', como o cuidado, sejam vistas com a mesma importância do que qualidades 'masculinas', como a tomada de decisões. eu não quero que mulheres sejam 'empoderadas' se isso significa ser como um 'homem' bem sucedido no sistema capitalista. foda-se esse sistema. 'força' nesse sistema significa poder, e poder significa subjugar quem não tem poder.

não é isso que eu quero. 

_quem não habita não tem hábitos_. e por isso não entendo a maioria dos costumes. ser mulher, ser homem. não faço ideia do que isso quer dizer. costumes são imposições, e eu boto na prateleira sem saber pra que serve. assim eu sou uma pessoa não-binária. os hábitos são quase sempre N/A -- não se aplicam. gênero, nesse sentido, não se aplica.
