---
title: " narrow-minded"
date: 2016-09-25 19:03:56
---

> My knowledge is limited, my mind puny. I tried hard, I stud­ied, I read many books. And nothing. In my home, books spill from the shelves; they lie in piles on furniture, on the floor, barring passage from room to room. I cannot, of course, read them all, yet my wolfish eyes constantly crave new titles. In truth, my feeling of limitation is not permanent. Only from time to time an awareness flares of how narrow our imagina­tion is, as if the bones of our skull were too thick and did not allow the mind to take hold of what should be its domain. I should know everything that's happening at this moment, at every point on the earth. I should be able to penetrate the thoughts of my contemporaries and of people who lived a few generations ago, and two thousand and eight thousand years ago. I should. So what?

Czeslaw Milosz, _Road-Side Dog_
