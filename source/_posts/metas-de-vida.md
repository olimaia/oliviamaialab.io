---
title: " metas de vida"
date: 2014-08-15 22:53:51
---

medir a preocupação pelo quanto me dói a artrose no maxilar. viver sem dor no maxilar. mais: um estilo de vida em que a única causa possível de dor muscular seja excesso de atividade no campo. anotar. não esquecer.
