---
title: " e essa chuva, hein?"
date: 2009-12-08 18:16:13
---

as estrelas e os revoltados do twitter que me desculpem, mas vou pisar outra vez no pé da relevância e mandar ela à merda, com muita classe e elegância, etc. porque se eu quiser escrever alguma coisa relevante (ai socorro) vou escrever no meu blog, que fica arquivado com alguma lógica, que fica fácil de achar, que fica bonitinho, que eu escrevo com quantas palavras meu eu mais prolixo bem entender. porque o twitter me serve pra dizer que estou com calor, que estou com preguiça, que estou com fome, que o ano passou rápido, que o gato está sentado em cima da minha pilha de trabalho etc etc etc. que talvez seja um jeito de dizer que o twitter não serve pra coisa nenhuma, mas, vai vendo, não fosse o twitter pra quem eu ia dizer esse monte de coisa irrelevante, se eu nunca encontro meus vizinhos no elevador, odeio o porteiro diurno (o noturno é incompreensível) e meus amigos brigam comigo quando eu falo sobre o tempo (_tudo isso é falta de assunto, Olivia?!_). e eu até gosto de falar sobre o tempo.
