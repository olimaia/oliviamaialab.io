---
title: "capitalismo de vigilância"
date: "2019-11-07 10:13"
---

em 2016 acertei na previsão do tema do ENEM. agora, em 2019, errei feio, como provavelmente erraram quase todos que tentaram um palpite.

em 2013 desconfiei que os protestos dos _20 centavos_ seriam inevitavelmente usados de forma torta, e à parte minhas ansiedades, e soube que não queria participar daquilo. no ano seguinte, escondida no norte da Argentina acompanhando os resultados das eleições para presidente, tive certeza de que não deixariam Dilma finalizar o mandato.

algumas coisas a gente pressente. a necessidade atual de se afastar de certos mecanismos das redes sociais e do mundo conectado. sair do facebook e do instagram e se livrar do smartphone. esconder-se na montanha no interior da Bahia e fazer artesanato, miniaturas, zines impressos para vender na feirinha de arte. ensaiar fugir também do whatsapp e do android, de uma vez por todas.

este site e blog que talvez ninguém mais leia, porque quem usa RSS hoje em dia.

desconfiar, questionar, duvidar. sempre.

também porque é impossível ler Shoshana Zuboff e seguir adiante como se nada houvesse acontecido.
