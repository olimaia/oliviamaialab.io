---
title: " algumas anotações soltas sobre o livro que estou escrevendo"
date: 2011-02-20 00:28:51
---

agora 170 páginas. mezzo encaminhado para o final. quer dizer, hora de parar de causar problema e começar a pensar em resolver o que pode ser resolvido.

é que já sei o final. mais ou menos. não sei ainda como é que vai tudo se dar até chegar nesse final.

tem um personagem que ficou rondando o livro inteiro e ainda não apareceu, e está querendo aparecer de qualquer jeito. vai aparecer, sim, daqui dois capítulo e até já sei qual a primeira coisa que ele vai dizer, que vai ser como se já estivesse no livro o tempo todo (calma, meu filho), mas de qualquer forma ele já está fazendo uma barulheira danada na minha cabeça.

as melhores ideias são as que não anoto. as menos burocráticas; quando as coisas se encaixam e aí anotar pra que, se já tudo caminha para isso.

penso às vezes que estou escrevendo esse livro só pra mim. que na verdade ele não vai interessar a mais ninguém. pode até ser.

preciso começar a pensar num título. por ora a pasta onde guardo o arquivo de texto se chama _agência_ (porque os personagens trabalham numa agência de investigações) e o arquivo de texto se chama _téo_ (porque o personagem principal se chama Juan Teodoro e, sim, ele me odeia por isso).
