---
title: " por um lugar para morar"
date: 2010-06-01 22:21:04
---

> In his text, the writer sets up house. Just as he trundles papers, books, pencils, documents untidily from room to room, he creates the same disorder in his thoughts. They become pieces of furniture that he sinks into, content or irritable. He strokes them affectionately, wears them out, mixes them up, re-arranges, ruins them. For a man who no longer has a homeland, writing becomes a place to live.

_Theodor W. Adorno_; traduzido do alemão por E. F. N. Jephcott.
