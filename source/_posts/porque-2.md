---
title: " por()que"
date: 2008-08-10 20:49:06
---

porque é preciso ser aquilo que os outros esperam de você. por que é preciso. por quê. 

? 

é preciso. quem é o outro? qualquer coisa que foi dita em coro por mais de meia dúzia de pessoas é mentira. auto-ajuda, senso comum. o certo e o errado. por que é preciso acreditar? por que é preciso compreender?
