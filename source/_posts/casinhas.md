---
title: mais rabiscos de casinhas
photos:
	- /img/originais/capture000027.jpg
	- /img/originais/capture000029.jpg
	- /img/originais/capture000030.jpg
date: 2020-10-05 13:29:21
---

umas casinhas novas na [página de ilustração](/arte/rabiscos) disponíveis pra download em alta resolução pra você imprimir e botar onde quiser. também estão disponíveis ainda os [originais](/arte/originais) dessas e de outras ilustrações e você pode pedir o que mais gostar e pagar quando quiser/puder.

aos curiosos fiz vídeos em _time-lapse_ do processo dessas (e de outras) ilustrações. tenho publicado tudo em meu [canal de artes do peertube](https://share.tube/video-channels/art_things). veja aí um deles:

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://share.tube/videos/embed/6c786543-9e7d-42a5-acc9-d22375512d08?title=0&warningTitle=0" frameborder="0" allowfullscreen></iframe>

RECOMENDO.

---

psiu: você pode [apoiar meu trabalho no catarse](https://catarse.me/oliviamaia)! não tem exclusividades pra assinantes porque meu objetivo é disponibilizar tudo de forma livre pra todo mundo, mas tem alegria e gratidão e você dá uma força pra uma artista independente continuar produzindo e espalhando arte por aí :)
