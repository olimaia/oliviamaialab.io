---
title: " bolívia, dia quatro: potosí-uyuni"
date: 2011-07-20 03:37:53
---

conclusão do dia: não faça de ônibus o que um jipe 4x4 foi feito para fazer.

estrada potosí-uyuni bloqueada. calhou de uma empresa estar fazendo o trajeto por um caminho alternativo, mais longo, mais caro. bora. saída de potosí às onze da manhã. primeira parte da estrada linda linda o ônibus e o céu a encosta e o abismo. a segunda parte, depois de o motorista parar para pedir informações, linda linda, de terra, chacoalhando que nem o diabo, o ônibus e o pó, só o pó. deos. que desespero.

dez horas de viagem, com direito a duas paradas no fim do mundo. chegamos passadas as nove horas, uyuni abandonada e escura, só o breu. achamos teto e cama junto de outro brasileiro da fflch e deixamos pra trás o chinês de taiwan -- ele não era coreano, afinal -- que preferiu ir noutro albergue. depois jantar num restaurante perdido no meio da escuridão. tá bueno, depois de almoçar duas maçãs, um club social, quatro oreos e um snickers.

amanhã tentar o tour pro salar, de dois dias, porque há neve por todo lado. no deserto. sim?
