---
title: " starbucks"
date: 2010-01-29 05:20:04
---

no starbucks do shopping todos os dias um senhor de bengala sentado em qualquer canto olhando sem muita discrição as menininhas dos colégios que gastam o dinheiro do pai com bebidas de café com chantily gigantescas. a mesma expressão de uma inércia terrível, e todos os dias o mesmo dia, e todas as menininhas as mesmas menininhas, os mesmos atendentes, a mesma chuva que cai, todos os dias. o mesmo agasalho azulado e desbotado os olhos de peixe por trás dos óculos e de repente se levantou e foi embora, para onde?

a ex-apresentadora de televisão que todos os dias um almoço solitário no viena e depois no café sentada olhando adiante lugar nenhum. ganha um muffin de chocolate do atendente mui simpático, porque o muffin saiu da caixa todo despedaçado e ele não pode vender aquilo mesmo. e chove todos os dias, não é mesmo, ela diz pra outra senhora que senta ali, e reclama que a gerente do lugar faz cara feia pra quem usa as poltronas sem consumir nada. tanta maquiagem e o cabelo impecável a expressão que não muda porque parece sempre tão brava e _com licença, senhora, qual seu nome?_; _quem quer saber?; __perdão, aquele senhor na outra mesa, ele se lembra da senhora, da televisão, e queria saber seu nome_. ora, porque muito orgulhosa, ou. levanta-se, aproxima-se e a expressão severa _Márcia Real_, o R reverberando de paulistana arcaica, antes que se volte à poltrona e que nada houvesse acontecido.

_todos os atendentes desse lugar são viados_, ele me diz. e deles a paciência porque eu _não posso café nem leite nem chocolate nem frutas ácidas_ e que tal uma bebida à base de chá, melhor framboesa que maracujá ou um suco de goiaba etc.
