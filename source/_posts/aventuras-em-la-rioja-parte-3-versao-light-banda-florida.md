---
title: "aventuras em la rioja, parte 3 (versão light): banda florida"
date: 2014-09-30 07:00:16
---

claro que depois da travessia até Laguna Brava a gente queria era descansar um pouco. gastamos a manhã conversando com o pessoal do hostel e não fazendo nada, e deixamos a tarde pra caminhar ali por perto, num bairro afastado chamado Banda Florida, que tem também uma pequena quebrada com um cânion do mesmo tipo de formação do Talampaya. caminhadinha de uns dois quilômetros até cruzar o bairro e chegar no pé do morro, pra depois se meter pela quebrada seguindo a marcação das fitas de saco plástico que indicam o caminho. depois de cada subida tinha ainda mais montanha adiante, e paramos quando cruzamos todo o cânion e avistamos a montanha que se ergue atrás dele, depois de um vale estreito.

passeio tranquilo depois da tormenta de neve do dia anterior, mas deu pra cansar; o dia que começou nublado abriu num sol forte no meio da tarde e definitivamente impossível entender o clima nessa região.
