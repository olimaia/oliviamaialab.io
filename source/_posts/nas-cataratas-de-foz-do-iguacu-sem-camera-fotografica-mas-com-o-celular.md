---
title: " nas cataratas de Foz do Iguaçu sem câmera fotográfica (mas com o celular)"
date: 2014-05-11 12:30:12
---

aí que eu fui visitar o parque das cataratas de iguaçu no lado brasileiro e quando cheguei fui tirar a primeira foto e me dei conta de que não tinha devolvido o cartão de memória à câmera. ops. passado o primeiro momento de desespero, decidi que ia curtir o passeio e tirar algumas fotos com o celular pra deixar registrada a visita, e pronto. paciência.

![](/img/posts/2014-05-09-10.53.20.jpg)

![](/img/posts/2014-05-09-11.17.07.jpg)

![](/img/posts/2014-05-09-11.50.53.jpg)

a visita se faz em mais ou menos duas horas, ao contrário do lado argentino que leva praticamente a tarde inteira. saí no final da manhã e atravessei a rodovia pra visitar o [Parque das Aves](http://www.parquedasaves.com.br). ali sim eu senti falta da minha câmera; uma das melhores coisas daquele 50x de zoom ótico é tirar foto de pássaros e bichos em geral. aí que primeiro achei a entrada do parque mei cara: 20 reais depois de já ter gastado 28 e uns quebrados na entrada das cataratas. depois achei que valeu a pena. o parque é incrível: ao longo do trajeto existem alguns viveiros nos quais você entra numa gaiolona gigante e fica ali a centímetros dos tucanos, das araras e dos outros pássaros de nomes e cabelos estranhos. os tucanos parecem se divertir dando rasantes temerosos a menos de um metro da sua cara. 

em um momento uma americana com uma câmera gigante me encarou com surpresa e uns olhos azuis esbugalhados: "where's your camera?!". respondi que tinha esquecido o cartão de memória e ela fez uma expressão de profundo pesar e um gesto de OH NOES. 

achei engraçada a prerrogativa dela, de que estar num lugar daqueles EXIGIA uma câmera fotográfica. eu gosto bastante de tirar fotos, principalmente de animais (e de pessoas), mas consegui vencer a frustração de não poder registrar aquela vista incrível das cataratas ou os gestos das corujas, por exemplo. e no final das contas tudo bem: a experiência é o que é pelo momento, e depois passa, como sempre. ficar sem a câmera, de certa forma, ajuda a valorizar esse momento. e afinal, por todo lado que eu vou a galera tem só o celular pra tirar fotos dos lugares. convenhamos que não é o fim do mundo não poder dar um super zoom de vez em quando. enfim, enfim. sobrevivi a um dia de cataratas e tucanos sem a câmera fotográfica. valeu principalmente pelos tucanos coçando o bico a meio metro de distância, seguindo com os olhos enquanto você passa devagar. eu, que em Alto Paraíso de Goiás só tinha conseguido ver um tucano no último dia, no começo da noite, meio ao longe, vi uns quinze voando de lá pra cá bem no meu nariz (e por pouco não levando junto o meu nariz, inclusive).
