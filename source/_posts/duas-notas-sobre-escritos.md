---
title: " duas notas sobre escritos recentes e minha negligência com as coisas importantes da escrita"
date: 2014-08-01 16:08:35
---

1. para um conto policial já tenho toda a estratégia narrativa e já sei o final e tenho alguma noção do crime central mas ainda falta o como e o porquê do crime central, donde: não consigo seguir para além da segunda página enquanto não souber essas duas coisas teoricamente **importantes**. 
2. parei o romance que estou escrevendo no meio de um diálogo com uma personagem secundária fazendo cara de preocupada e se preparando para fazer alguma revelação **importante** mas: não lembro o que ela ia dizer.
