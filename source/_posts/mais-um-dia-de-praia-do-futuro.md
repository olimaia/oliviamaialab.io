---
title: " mais um dia de Praia do Futuro"
date: 2013-08-11 22:48:35
---

mais um dia de sol e 30 graus e vento. também dia dos pais e claro que a barraca na Praia do Futuro estava bem cheia. depois de brigar bravamente com um carangueijo, fomos para um mergulho no mar. tinha se formado uma espécie de piscina gigante entre a areia e uma faixa mais rasa uns vinte metros mar adentro, antes da arrebentação. porque o mar na Praia do Futuro é bem brabo (e o vento também), então aquele pedaço de água rasa e sem onda estava bem bom para ficar um pouco feito lontra. tinha um casal de velhinhos adolescentando por ali, brincando de boiar e jogar água um no outro, tão lontras como nós. 
