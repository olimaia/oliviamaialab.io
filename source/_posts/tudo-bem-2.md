---
title: " tudo bem"
date: 2012-12-11 08:56:01
---

pode olhar torto e dizer que _você vai precisar de dinheiro de qualquer jeito_, feito fosse gerente de banco e paladino da máxima sensatez, ou essas risadinhas que a gente reserva para as avós a quem já escapa a lucidez e as crianças que sonham em comprar um zoológico. pode não levar a sério e pensar que eu enlouqueci, que é uma fase, que vai passar. ter certeza de que vou recuperar a sanidade e desistir antes mesmo de tentar. pode dizer que o mundo é assim e dar de ombros tão conformadamente. pode dizer que não podemos mudar o sistema e pode até mesmo dizer que _não é tão ruim assim porque afinal as exceções etc_. vai falando que eu não estou ouvindo, que estou eu aqui com a minha risadinha que a gente reserva aos velhos mais ricos e sozinhos, às filas de rua para comprar celular novo, ao trânsito na rebouças e aos executivos discutindo estratégia de marketing e valor agregado no horário de almoço.
