---
title: " da literatura"
date: 2008-05-09 04:41:21
---

e me encanta a literatura pelo tanto dela que não podemos compreender, que jamais se poderá compreender; que é o que fazem os poetas -- que são todos os escritores -- quando tentam capturar com palavras o inexistente, o inexprimível e o impossível, como fez Alice ao atravessar o espelho, não saltando sobre ele, mas sim diluindo-o com a palavra, horizontalmente atravessando-o como se nunca pudesse ter havido ali qualquer tipo de oposição.

deparar-se com esse choque, com o que é a ficção e se mescla ao real, com essa possibilidade inexplicável de criar-se a si mesmo e transformar os possíveis. essa literatura que é sempre o Odradek, esse ser efêmero de forma ao mesmo tempo precisa e incerta -- impossível -- que fala em um sopro sem pulmões, porque a idéia de que ele possa nos sobreviver é eternamente dolorosa para mim.
