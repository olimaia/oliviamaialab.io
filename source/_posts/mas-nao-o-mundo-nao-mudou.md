---
title: " mas não: o mundo não mudou"
date: 2016-10-17 17:56:38
---

> Mas é uma sociedade perfeita para ser governada; e a prova disso é que todos os que aspiram a governar querem governa-la, pelos mesmos procedimentos, e mantê-la quase exatamente como ela é. É a primeira vez que, na Europa contemporânea, nenhum partido ou fração de partido ensaia somente pretender que tentaria mudar qualquer coisa de importante. [...] Em todo o lado onde reina o espetáculo, as únicas forças organizadas são aquelas que querem o espetáculo. [...] Acabou-se com esta inquietante concepção que dominou durante mais de duzentos anos, segundo a qual uma sociedade podia ser criticável e transformável, reformada ou revolucionada.

Guy Debord, _Comentários sobre "A sociedade do espetáculo" _
