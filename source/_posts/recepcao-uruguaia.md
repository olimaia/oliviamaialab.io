---
title: " recepção uruguaia"
date: 2014-06-03 18:57:32
---

depois de uma viagem que incluiu uma parada pra trocar um pneu furado do ônibus, fui recepcionada ainda na rodoviária do Chuí pelo Juan, do couchsurfing. (tinha perguntado pro cobrador do ônibus pra que lado era o Uruguai e ele: duas quadras pra lá.) Juan me levou pra comprar pesos e um chip pré-pago da Antel. seguimos pra Barra del Chuy, passando pela aduana pra que eu entrasse oficialmente no país. dia de sol, céu azul, mar azul, vento gelado. bah. na casa do amigo rolou um asado uruguayo. melhor recepção. depois ainda vimos na tv (globo com Galvão Bueno) o jogo do Brasil. logo vou com o Juan e a Fran pra casa deles em San Luis al Medio, a 30km daqui. vou matando um tempo numa padaria enquanto eles estão na aula de ioga.
