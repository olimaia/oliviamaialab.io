---
title: "Aline Valek: Leitura do desconforto"
date: 2014-08-01 16:24:11
---

> O que chamou a atenção de cara foi a escrita de Olivia: frases que terminavam pela metade, palavras que ficavam gritando justamente pela sua ausência, no que pode parecer a alguns um descuido na revisão, mas que, na verdade, constituem um trabalho cuidadoso da autora em construir uma narrativa baseada em lacunas. É uma história cheia de mistérios, ué! Logo, é sobre algo que não está presente, sobre o não-contado, o não-dito.

Resenha de “A última expedição” publicada pela Aline Valek em [seu blog](http://www.alinevalek.com.br), em 10/07/2014. [Leia o post](http://www.alinevalek.com.br/blog/2014/07/leitura-do-desconforto/).
