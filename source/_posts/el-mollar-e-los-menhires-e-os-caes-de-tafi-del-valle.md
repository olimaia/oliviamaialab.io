---
title: " el mollar e los menhires e os cães de tafí del valle"
date: 2014-10-08 12:36:46
---

a caminhada de Tafí até o povoado de El Mollar são, teoricamente (duvido), 7 km. por dentro pelo rio tem um caminho de terra que o pessoal faz de bicicleta, mas tem ainda a opção de ir pela estrada do outro lado. claro que fomos pelo caminho de terra: eu e Andressa, Marcela e o portenho. e três cães.

desde o começo veio uma que vive no hostel embora ninguém nunca a tenha convidado; depois eu ia saber que o nome dela é Lola. também um branco com pinta de importante e outro negro e marrom. pelo caminho os cães entravam e saíam do grupo.

quando cruzamos a ponte eram uns quatro mas outro deles ficou pelo caminho. que é um caminho lindo. e longo. mas: lindo. 

o rio pela esquerda, os verdinhos de começo de primavera, as casinhas de adobe na beira da estrada (se é que pode chamar de estrada), o dique. as montanhas por toda parte e El Mollar ao fundo sem parecer nunca ter ficado mais próximo. erramos caminho uma vez, meti a bota num lamaçal, voltamos, encontramos a trilha, perdemos uns 20 ou 30 minutos nessa volta mei inútil. tivemos algumas vezes que defender os cachorros que iam se metendo em território que não lhes diz respeito. em um momento um grupo de uns dez cães correu atrás do cão branco até ele sumir de vista e ficamos sem saber que cazzo tinha acontecido (digamos que os cães não estavam muito bem intencionados). Lola que tinha desaparecido no meio desse quilombo reapareceu alguns minutos adiante como se nada houvesse acontecido e ainda foi com a gente o resto do caminho, esperou enquanto comíamos qualquer porcaria num restaurante da praça e ainda visitou a reserva arqueológica Los Menhires. 

a reserva é uma coleção desses totens de pedra de diversos lugares, que em muitos momentos, principalmente durante a ditadura, foram trasladados pra lá e pra cá. por isso estão fora de contexto, mas pelo jeito foi o melhor que puderam fazer. quando chegamos na praça pensando em perguntar onde ficava a reserva olhamos pro lado e lá estava; é um terreno pequeno em pleno centro do povoado e a entrada é grátis (e entram os cães como se nada). depois caminhamos até o dique mas um vento cruel e gelado nos impediu de matar muito tempo por lá. de qualquer forma já chegava o horário do ônibus de linha que nos deixaria de volta em Tafí del Valle. 

esperamos na praça com a molecada que saía da escola. subimos no ônibus e Lola (e outros dois cães que já tinham se unido ao nosso grupo) ficaram nos olhando com expressões desoladas. mas caminhar outros 7 km (provavelmente 8) estava mei fora de cogitação. voltamos pra capotar no hostel e aproveitar a janta incluída na diária. (e adivinha quem já estava na porta do hostel na manhã seguinte?)
