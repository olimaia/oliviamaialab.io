---
title: " por pertencer"
date: 2010-05-11 17:37:45
---

Cria paulistana súbito o céu de inverno à noite, as estrelas. Na escuridão do universo tantos os pontos de luz, piscando sem descanso, como por certeza de que não se está sozinho, de que algo maior observa, e tantos os pequenos pontos, infinitos, eles todos sóis, centro de gravidade. De uma insignificância o maior da existência, por pertencer. A algo se pertence. Algo. E as estrelas que olham, por qualquer coisa a se dizer. Ainda no breu uma faixa de névoa luminosa. Os olhos míopes que mesmo através dos óculos ainda uma imprecisão das formas, a nitidez impossível ou o grau que enfraqueceu. Abaixo as lentes e maior a névoa, imagem que se dissolve.
