---
title: " fora de hora, Cortázar eterno"
date: 2010-05-24 14:48:49
---
  
o título tão certo, por abraçar todos os contos; Deshoras, esse desencontro, esse chegar tarde demais ou no momento errado, essa incapacidade de se conciliar com a passagem do tempo, dos segundos, minutos, essa constatação da incapacidade de se acertar o passo da vida ao passo do relógio, que gira negligente aos apelos humanos. a necessidade de se narrar como tentativa última de apreender o inapreensível, e outra constatação: a impossibilidade do narrar que se transforma no texto cheio de voltas e vírgulas e adiante adiante adiante o texto se volta sobre si mesmo mas nem ele mais é capaz de voltar, de compreender, de descrever, de narrar. tudo imagens que nos escapam e que o texto costura, imagens fora de hora, fora do tempo, pregadas amarradas no papel por uma Olympia Traveller de luxo. _fora de hora_ que é também o título de um dos contos, deles o que mais tapa na cara pelos desencontros do caminho, e a tentativa sempre frustrada de escapar à crueza da cronologia por meio de palavras inventadas.
