---
title: " o terror da beleza"
date: 2016-04-25 10:48:29
---

> o terror da beleza, isso, o terror da beleza delicadíssima tão súbito e implacável na vida administrativa

Herberto Helder (em _Servidões_)
