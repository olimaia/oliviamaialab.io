---
title: " de Tibagi a Foz"
date: 2014-05-09 16:39:28
---

saí de Tibagi no sábado, depois de fazer a trilha no parque estadual do Guartelá. o cânion não é grande coisa, verdade, mas a trilha é gostosa de fazer e também o passeio guiado não é muito caro: 28 reais, sem o transporte (fui com um casal que também ia fazer a trilha). 

fiquei pela cidade até a hora do último ônibus pra Ponta Grossa: 17h. é uma viagem bem cansativa porque o ônibus é um ônibus urbano comum e são quase duas horas. cheguei cansada e ainda tinha umas 3 horas pra matar até meu ônibus pra Foz, que saía às 22h15. enfim enfim. graças ao antialérgico que estava tomando desde Tibagi (foi chegar no frio do interior do Paraná que fiquei toda alérgica) dormi praticamente a viagem inteira. cheguei em Foz de manhãzinha e tomei um ônibus da rodoviária ao terminal de transportes urbano (TTU). de lá até o Hostel Chili foram só alguns quarteirões. por sorte a cama já estava liberada e o Lee, o dono do albergue, me deixou ocupar o quarto antes do horário do check-in. ufa ufa. descansei um pouquinho, mas logo estava conversando com um australiano que estava por aqui. acabamos saindo à tarde para visitar o templo budista. tomamos um ônibus no TTU e em pouco mais de meia hora estávamos lá. 

o hostel é mui tranquilo e o dono bastante prestativo. deu pra fazer amizade com a turma que estava passando por aqui. logo conto das visitas às cataratas (sensacional) e à usina de Itaipu.
