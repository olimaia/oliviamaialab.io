---
title: " Icho Cruz, Tala Huasi e o rio San Antonio"
date: 2014-09-02 14:30:20
---

no domingo (24 de agosto) depois da excursão fui conhecer o povoado de Icho Cruz e o rio que passa por ali. fica tudo meio colado indo mais ou menos pra sudoeste de Carlos Paz: San Antonio de Aredondo, Mayu Sumaj, Villa Icho Cruz, Cuesta Blanca. pro norte do rio por Icho Cruz tem ainda outra comuna (comuna são as localidades que nem povoados são e por isso não têm intendencia), mas ainda assim são independentes), chamada Tala Huasi. fui com uma moça que conheci pelo Couchsurfing, numa postagem perguntando por companhias pra caminhadas pela região. ela é de Buenos Aires mas está há alguns meses morando em Villa Allende, ao norte da cidade de Córdoba. tomamos o ônibus na rodoviária de Carlos Paz e descemos no que devia ser a parada principal de Icho Cruz. seguimos em direção ao rio e cruzamos pra Tala Huasi.

ali um monte de casinhas de veraneio e chalés. e ninguém nas ruas. ninguém nas casas, também. o vento frio depois daquela inesperada chuva na sexta-feira anterior. mas o lugar uma lindezinha. o rio, como todos parecem ser aqui pela região, de uma água transparente. nos seguia sempre algum cachorro.

fizemos uma parada à beira do rio para um mate e depois de rodar bastante (sobe morro desce morro passando por o que deve ser um bairro novo em construção) fizemos uma segunda parada para almoçar, também em outro ponto de balneário. tem mais fotos no álbum dos arredores de Carlos Paz. o Pablo tinha me dito que Icho Cruz era desses _pueblos de ruta_, mas a verdade é que gostei demais do lugar. foi pra minha lista de lugares pra morar. a lista está crescendo.
