---
title: " essas voltas policialescas"
date: 2013-07-08 21:30:25
---

vai indo tudo muito bem esse meu afastar-se da ficção policial, lentamente, gradativamente, levando o que dela me interessa e deixando de lado as burocracias da verossimilhança, que tão mais me incomodavam quanto mais eu aprendia sobre a polícia brasileira. mas é que basta assistir a um episódio de qualquer seriado policial (desses americanos, porque foda-se a verossimilhança) e me volta essa gana que começou tudo e me fez trabalhar com o gênero desde o começo, sem que eu nunca soubesse muito bem por quê. ou porque criminosos homicidas moram em mim, e é preciso se livrar deles de alguma forma?
