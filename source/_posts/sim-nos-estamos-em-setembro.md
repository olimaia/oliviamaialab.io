---
title: " sim, nós estamos em setembro"
date: 2009-09-09 02:26:21
---

e na Teodoro Sampaio descia uma _enxurrada_ terrível que subia até o tornozelo de qualquer incauto passando pela _calçada_, enquanto a solução era esperar o ônibus dentro das lojas, e _com_ o guarda-chuva aberto.
