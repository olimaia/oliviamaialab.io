---
title: " os gatos e a metafísica"
date: 2016-02-23 12:44:11
---

por não exigir muito mais além do que existe; que todos os objetos ao seu alcance são seus brinquedos. porque em uma semana se nota que o bichinho cresceu e você nem fez muito além de pôr comida, trocar a água, limpar a areia. deixar dormir em cima da cama, bem no meio entre um travesseiro e outro. por dizer que não se precisa muito mais do que isso: comida, água, teto, amor. que tanto a gente complica, espera, exige meu deus.
