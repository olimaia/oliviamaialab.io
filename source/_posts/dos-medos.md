---
title: " dos medos"
date: 2008-04-29 03:35:22
---

na verdade quanto mais convivo com professores e ouço de professores que há muitos anos dão aulas -- e veja que estou falando de escolas particulares -- mais me parece assustador dedicar a vida a esses projetos-de-gente que são os adolescentes (e as crianças), e deixar-se despedaçar por esse eterno reencontrar-se com o passado, o eterno reinventar-se para ser percebido, e o choque das gerações que passam a viver juntas ao mesmo tempo dentro de você.

mas então, eu...?
