---
title: " outridade"
date: 2011-03-07 22:44:41
---

> “_No fundo_, poderíamos ser como na superfície”, pensou Oliveira, “mas teríamos que viver de outra maneira. E o que quer dizer viver de outra maneira? Talvez viver absurdamente para acabar com o absurdo, sair de si mesmo com tal violência que o salto acabasse nos braços de outro. Sim, talvez o amor, mas o _otherness_ dura para nós o que dura uma mulher, e além disso só no que toca a essa mulher. No fundo, não há _otherness_, apenas o agradável _togetherness_. É certo que isso já é alguma coisa...” Amor, cerimônia ontologizante, doadora de ser. E por isso lhe ocorria agora aquilo que na verdade lhe deveria ter ocorrido logo no início: sem se possuir a si mesmo, jamais poderia possuir a outridade. E, afinal, quem é que se possuía de verdade? Quem é que tinha a perfeita consciência de si mesmo, da solidão absoluta que significa ter de entrar num cinema ou num bordel, ou em casa de amigos ou numa profissão absorvente ou no matrimônio para estar pelo menos só-entre-os-demais? Assim, paradoxalmente, o cúmulo da solidão conduzia ao cúmulo do gregarismo, à grande solidão das companhias alheias, ao homem só na sala de espelhos e dos ecos. Todavia, pessoas como ele e tantas outras, que se aceitavam a si mesmas (ou que se repeliam, mas conhecendo-se de perto), entravam sempre no pior paradoxo, estar talvez à beira da outridade e não poder alcançá-la. A verdadeira outridade feita de delicados contatos, de maravilhosos ajustes com o mundo, não podia ser cumprida por um só lado: a mão estendida deveria receber outra mão, vinda de fora, vinda do outro.

Julio Cortázar _O jogo da amarelinha_, capítulo 22

quanto mais leio (releio) esse livro mais eu quero entrar dentro dele e não sair nunca mais.
