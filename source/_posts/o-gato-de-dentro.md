---
title: " o gato de dentro"
date: 2010-05-03 16:13:56
---

não sei nada de beatniks ou o que seja, mas li agora um livro de "memórias" do William Burroughs chamado _The cat inside_, e achei uma delicinha. é um livro curtinho, caro demais pro seu tamanho, mas comprei* nessa última promoção de livros importados da Livraria da Vila, então OK. *e porque trabalhei loucamente no mês passado, me parece justo gastar uma parte das correções com livros delicinha.

> Purring in his sleep, Fletch stretches out his little black paws to touch my hands, the claws withdrawn, just a gentle touch to assure him that I am there beside him as he sleeps. He must have a dream image of me. Cats are said to be color-blind: grainy black-and-white, a flickering silver film full of rents as I leave the room, come back, go out, pick him up, put him down. Who could harm such a creature? Train his dog to kill him! Cat hate reflects an ugly, stupid, loutish, bigoted spirit. There can be no compromise with this Ugly Spirit.

que se pode fazer? gatos são uma dessas coisas encantadas que existem no mundo. o que há de felino nos gatos nos faz mais humanos. _the cat inside_ é também um pouco dessa nossa humanidade perdida. através dos séculos.

_we are the cats inside. we are the cats that cannot walk alone, and for us there is only one place._
