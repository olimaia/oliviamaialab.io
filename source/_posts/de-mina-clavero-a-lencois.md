---
title: " de mina clavero a lençóis"
date: 2015-12-07 14:00:28
---

sete de dezembro: um ano juntos, com voltas pela Patagônia, pelo nordeste brasileiro, pela Suíça e Alemanha e um pedacinho de leste europeu. quanta coisa dá pra fazer em um ano meu deus. 

deu pra passar frio em barraca na noite de verão patagônica, nadar em lago de desgelo, cozinhar com lenha porque não tinha gás, guerrear contra pernilongos na porta da barraca, tomar cerveja na praia em dia de semana, fazer amizade com os cachorros da rua, comprar passagem de avião na última hora e não dormir à noite de ansiedade, passar calor em Zurique, ter intoxicação alimentar no sul da Alemanha, visitar castelos, não entender nada nos cardápios em tcheco, aprender a pedir cerveja em polonês, escalar na Eslováquia, aprender um pouco de alemão e aprender um monte de português, esquecer um monte do espanhol. deu pra caminhar muito, no asfalto na terra na areia na pedra e na neve, subir e descer montanhas e vulcões, se perder, inventar caminhos, gastar muita sola de sapato e criar muito calo nas mãos escalando (sem contar os arranhões e cortes e aquela queda no interior do Ceará). deu também pra sofrer um pouco por causa de burocracia e prazo de visto e conseguir passar por cima de tudo isso. 

começamos em Mina Clavero, cidadezinha da província de Córdoba, na Argentina. fomos parar em Lençóis, interior da Bahia. dezembro de 2014 parece que foi há uma eternidade. agora que venha o próximo ano neste exercício de voltar a criar raízes, e daí vemos onde mais a gente vai parar.
