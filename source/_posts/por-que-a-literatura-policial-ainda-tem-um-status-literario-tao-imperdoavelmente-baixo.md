---
title: " por que a literatura policial ainda tem um status literário tão imperdoavelmente baixo?"
date: 2009-07-29 00:37:23
---

> At its best, crime writing offers unique insights into society, psychology and human behaviour. It can be both engaging and literate; compelling and well-written. It can be innovative and surprising, but what it can't be, it seems, is feted in the same way as literary fiction. The most a crime writer can hope for is to be told, as Ian Rankin indeed was, that their novels "almost transcend their genre". Faint praise indeed.

[Stuart Evers, no Books Blog do Guardian.co.uk](http://www.guardian.co.uk/books/booksblog/2009/jul/28/crime-low-literary-status). sempre um questionamento a ser feito.
