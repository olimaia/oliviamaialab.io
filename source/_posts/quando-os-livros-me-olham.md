---
title: " quando os livros me olham"
date: 2011-06-14 14:51:00
---

vai que tenho lido quase nada. escrito quase nada, também. trabalho ocupa o tempo, mas constrói um vazio.

aí que acabou o semestre para o trabalho #1 e agora só em agosto. cresceu o tempo. cresceu o vazio.

olho os livros nas minhas estantes e não tenho vontade de pegar nenhum deles. desconfio de todos. _acho que não é bem isso que eu queria ler_. nem mesmo na livraria. terminei de ler um Calvino e concluí que de fato gosto mais do Calvino mais metalinguístico. quer dizer, Calvino é sempre metalinguístico, mas quando foge demais na viagem, se mete por caminhos obscuros, me perco, desinteresso. vejo minha pilha cada vez mais baixa de Cortázar por ler. penso que _melhor deixar para depois_. senão acaba. tenho vontade de reler _O jogo da amarelinha_. mas tantos livros por ler.

aí que penso em ler _Tender is the night_, do Fiztgerald. é o último que me falta, dos romances. não é também o que eu queria ler, mas é uma história, e ando precisando de personagens, desses com nome e personalidade, não essa coisa moderninha de gente chamada F ou K etc. gente que não é gente. ou Bolaño, com aquele monte de nome, aquele monte de gente que passa feito fazendo vitrine. mas aí me encara _Os detetives selvagens_ e pergunta se _e aí, vai encarar?_, mais dois Javier Marías, um Tomás Eloy Martinez, um Mario Benedetti. olho na prateleira de cima os policiais em inglês e na prateleira de baixo os infinitos Simenon e penso que queria mesmo era ler um de espionagem dos antigos do Le Carré. mas os dois que tenho eu já li. e dizem que são os melhores.

ou escrever. mas a burocracia, o minc, as incertezas, essa viagem incerta que fica cada vez mais próxima e ainda não posso comprar as passagens. que desânimo.

a ficção me faz uma falta. que chatice sem fim esse mundo linear de não-ficção.
