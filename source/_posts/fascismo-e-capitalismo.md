---
title: " fascismo e capitalismo"
date: 2017-01-17 13:20:03
---

> Aqueles que estão contra o fascismo sem estar contra o capitalismo, que choramingam sobre a barbárie causada pela barbárie, assemelham-se a pessoas que querem receber a sua fatia de assado de vitela, mas não querem que se mate a vitela. Querem comer vitela, mas não querem ver sangue. Para ficarem contentes, basta que o magarefe lave as mãos antes de servir a carne. Não são contra as relações de propriedade que produzem a barbárie, mas são contra a barbárie.

Bertolt Brecht, _As cinco dificuldades de escrever a verdade_
