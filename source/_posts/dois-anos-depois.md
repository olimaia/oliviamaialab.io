---
title: " dois anos depois"
date: 2015-08-07 14:07:52
---

... há exatamente dois anos tomei um avião pra Fortaleza e comecei essa tal _viagem_. tenho agora mais um avião pra Fortaleza, pro dia 29 de setembro, que é pra começar a terminar a viagem. ou. já nem sei como melhor chamar tudo isso. talvez seja melhor chamar de vida. estou agora no sul da Alemanha onde eu jamais imaginaria que estaria dois anos depois. um dia de castelos e esquilos. entendendo uns 60% de alemão. ou uns 80% quando o cidadão tem a bondade de falar devagar sem esse sotaque preguiçoso da Bavaria. reviravoltas.

(os esquilos obviamente não se deixaram fotografar.)
