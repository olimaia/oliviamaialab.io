---
title: " joana"
date: 2016-04-11 17:21:24
---

uma amiga da minha mãe tinha dito que um casal de velhinhos da vizinhança morreu e deixou uma gata que os filhos não podiam cuidar. fui com minha mãe. uma vizinha abriu a porta dos e Joana entrou pra esperar a comida que sempre vinha. peguei Joana no colo e ela só fez se acomodar e ronronar. disse à minha mãe: então, vamos? não teve outra alternativa. diz-se que o nome dela era Nina; mas em casa a cachorra já era Nina, então já no carro ela ficou Joana. ela tinha cara de Joana. 

muito rápido se fez rainha e brigava com todos os outros animais da casa. com pessoas, não: fazia amizade com o encanador, o eletricista, o funcionário da net, o vizinho, o zelador. Joana provavelmente pensava que era também um pouco humana. em seu infinito mau humor, no fundo ela gostava da companhia dos bichos: de bater no cachorro, de ficar parada na porta da cozinha pra impedir a cachorra de passar, de reclamar com o gato Teodoro quando ele ousava brincar perto dela. era só ficar um pouco mais sozinha que ela já chamava pra saber se alguém estava por perto. e nas vezes que voltei de viagem passava sempre três dias me ignorando, de costas, pra me fazer entender que ela estava aborrecida com minha ausência; então vinha e subia em cima de mim, ronronando. nunca soube quantos anos ela tinha. provavelmente muito mais do que pensei.

que eu posso dizer? nunca precisei dizer muito pra ela. este post é na verdade só um agradecimento por ter tido a companhia dessa gata rabugenta por tantos anos. <3 Joana. 
