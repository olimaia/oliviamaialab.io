---
title: " ninguém, nunca"
date: 2010-12-09 12:10:00
---

> Para chorar, dirija a imaginação a você mesmo, e se isso lhe for impossível por ter adquirido o hábito de acreditar no mundo exterior, pense num pato coberto de formigas e nesses golfos do estreito de Magalhães _nos quais não entra ninguém, nunca_.

em "Instruções para chorar", do Cortázar,

no livro _Histórias de cronópios e de famas_.
