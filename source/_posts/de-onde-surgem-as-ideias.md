---
title: " de onde surgem as ideias"
date: 2014-01-20 10:33:39
---

a pergunta mais ouvida pelos escritores (e quando digo escritores, não estou falando dessa gente de livro publicado e noite de autógrafos, mas qualquer um que se arrisque a inventar algumas narrativas ou versos e tenha a audácia de declarar isso para alguém): de onde você tira suas ideias? como se ideia fosse uma coisa-coisa, um troço meio amorfo ou talvez meio cilíndrico, uma caixinha que a gente pega n'algum lugar com uma etiqueta amarela na tampa. mas é que as ideias a gente não tira; as ideias surgem, as ideias a gente encontra, tropeça nelas, a qualquer momento. andando na rua, por exemplo. capoft: era uma ideia. ops. às vezes passam meses ausentes, escondidas. depois uma delas resolve dar as caras, com a desculpa de que se atrasou porque se perdeu no aeroporto de Frankfurt (ouvi dizer que isso é comum). as ideias aparecem coloridas no meio de um devaneio pré-sono (e te deixam acordado até às quatro da manhã): uma frase, um rosto, uma sensação. assim, do nada. juro. as ideias surgem do nada.
