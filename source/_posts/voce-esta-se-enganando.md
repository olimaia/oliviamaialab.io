---
title: " você está se enganando"
date: 2016-02-29 12:44:11
---

você está fugindo de você mesmo; você está fugindo dos seus problemas. dito por aqueles que na verdade queriam dizer _você não está fazendo as coisas como eu queria que você fizesse_.
