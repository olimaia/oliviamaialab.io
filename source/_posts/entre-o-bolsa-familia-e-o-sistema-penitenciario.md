---
title: " entre o bolsa família e o sistema penitenciário"
date: 2017-01-19 15:17:34
---

> Aqueles que a expansão da liberdade do consumidor privou das habilidades e poderes do consumidor precisam ser detidos e mantidos em xeque. Como são um sorvedouro dos fundos públicos e por isso, indiretamente, do "dinheiro dos contribuintes", eles precisam ser detidos e mantidos em xeque ao menor custo possível. Se a remoção do refugo se mostra menos dispendiosa do que a reciclagem do refugo, deve ser-lhe dada a prioridade. Se é mais barato excluir e encarcerar os consumidores falhos para evitar-lhes o mal, isso é preferível ao restabelecimento de seu _status_ de consumidores através de uma previdente política de emprego conjugada com provisões ramificadas de previdência. E mesmo os meios de exclusão e encarceramento precisam ser "racionalizados", de preferência submetidos à severa disciplina da competição de mercado: que vença a oferta mais barata...

Zigmunt Bauman, O mal-estar da pós-modernidade
