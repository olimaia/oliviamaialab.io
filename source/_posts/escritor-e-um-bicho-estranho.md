---
title: " escritor é um bicho estranho"
date: 2016-01-03 16:09:06
---

escritor é um bicho estranho. escritor trabalha primeiro, e depois torce pra, talvez, ser remunerado. escritor na internet é ainda mais estranho: trabalha trabalha trabalha loucamente, de graça, e de vez em quando pede uns trocados; se tanto, fica esperando que todo esse capital simbólico se transforme em um convite pra escrever e ser pago por isso. muitas vezes o que recebe são convites pra trabalhar de graça. mas essas pessoas pra quem ele trabalha de graça estão ganhando alguma coisa? ou é porque estão trabalhando _de verdade_? mas na maioria das vezes o escritor segue escrevendo; segue trabalhando em troca de capital simbólico; segue trabalhando um _day job_ qualquer que ponha comida na mesa e que muitas vezes serve também pra lhe tirar o fôlego e a vontade de fazer o trabalho que ele realmente queria estar fazendo. não tenho dúvida de que a lógica atual do trabalho é torta, mas que estranho pensar que, quando se trata de um escritor, tudo bem que essa lógica não se aplique. tudo bem que ele não receba nada pra trabalhar, que ele tenha que ter _dois_ trabalhos, um que paga e outro que... não é nem um trabalho, afinal. todo mundo sabe escrever, não é mesmo? que mundo louco esse que manda o escritor _trabalhar_ e não quer pagar o escritor pelo trabalho que ele produz. e que vamos dizer do artista? o que é o mundo sem a arte? sem a literatura? mas a gente segue trabalhando, de graça, por amor, pela necessidade de conectar-se, de expressar-se, pelo capital simbólico que às vezes se transforma em capital financeiro e ajuda a pagar as contas, mas nem sempre. quase nunca. porque eu, por mim, trabalharia de graça mesmo; escreveria pro resto da vida sem nunca pedir nada em troca. não fosse o mundo exigir que eu pague por teto, comida, internet. o mundo é um lugar estranho.

* * *

*da newsletter da Aline Valek [um texto sobre o trabalho do escritor dentro da lógica do trabalho](http://us3.campaign-archive1.com/?u=3d132d648909c355a3e38e32e&id=12637c8648) (o trecho começa mais pro final, ali onde diz "Imagine que você chegue numa entrevista de emprego e ofereçam a seguinte vaga pra você ...").

*Clara Averbuck [pergunta se "paga?" ou se o jeito é arrumar um "emprego de verdade"](https://medium.com/@claraaverbuck/paga-43388468ae60#.9qfaje3ku).

* * *

se você gosta do que eu escrevo, se você gosta das minhas fotos, se você quer me incentivar a continuar escrevendo _de graça_, no blog e na newsletter, considere **fazer uma colaboração**. você pode fazer [uma contribuição única de qualquer valor ou uma assinatura mensal de _dez reais_](http://oliviamaia.net/colabore/). _dez reais_ é o preço de uma cerveja. você me ajuda a pagar as contas e eu posso me dedicar à escrita com mais tempo e tranquilidade. e mais uma vez feliz ano novo!
