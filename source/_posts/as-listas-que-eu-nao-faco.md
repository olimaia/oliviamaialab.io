---
title: " as listas que eu não faço"
date: 2017-01-03 18:58:57
---

**livros que eu quero ler.** porque são tantos. quantos livros um ser humano normal consegue ler em um ano? em 2016 eu li muito, mas ainda assim muito pouco. a pilha de livros que pretendo ler _em breve_ já vai pra mais de 140. não vou escolher entre eles uns 60 ou 80 pra ler neste ano que começa. vou mudar de ideia. vou descobrir qualquer coisa mais urgente ou mais interessante ou urgente e interessante. que tem sido a melhor maneira de ler: terminar um livro e fuçar na estante (física ou virtual) qual título grita mais alto. 

**resoluções de ano novo.** a pessoa ansiosa pode fazer listas de coisas do dia: lavar louça, arrumar a cama, buscar encomenda no correio, pagar a conta da internet. lista de COISAS pra fazer ao longo do ano é pedir pra construir ansiedade enquanto os itens ficam ali parados todos os dias olhando pra você como quem diz _hoje só amanhã?_ 

**as listas _para o futuro_.** por que nos maltratamos tanto? faço listas infinitas de autores e livros pra procurar. guardo artigos no pocket pra ler _um dia_. ou, quem sabe, pra um dia começar a ler e nem lembrar mais por que eu tinha guardado aquele troço. faço listas de _coisas que preciso lembrar_. listas de compras no mercado. o velho método de anotar as coisas pra não precisar ficar guardando na cabeça. às vezes elas ficam tanto tempo anotadas que umas semanas depois elas perdem toda importância. talvez seja esse o principal objetivo das listas.
