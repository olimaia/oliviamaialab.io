---
title: "on a side note"
date: 2009-06-26 04:44:29
---

mortes e crises mundiais à parte, minha contagem regressiva: . . um trabalho sobre as diferenças entre o sofista e o filósofo no diálogo Protágoras do Platão pra tentar fazer em dois dias, no máximo. . . um resumo de algum ensaio do Antonio Candido [escolhi o "Jagunços mineiros de Cláudio a Guimarães Rosa"], pra entregar no dia 30 de manhã. . . 50 redações de recuperação pra receber na segunda e corrigir até terça. . . um trabalho de metodologia do ensino do português pra finalizar, botando uns dados pra deixar a professora feliz. tudo isso porque greve pra lá e pra cá e melhor mesmo é terminar tudo isso antes da minha viagem pra Fortaleza pra visitar o pai no dia 3. e volto no dia 13. ou seja: tenho sete dias pra dar um jeito nisso. ou seis, porque a quinta-feira dia 2 não conta. e torcer pra professora de metodologia não inventar encontro na primeira semana de julho, que aí fodeu. aliás, botar na lista: mandar email pra professora de metodologia e dizer que, pelo amor de deus, deixa eu viajar e depois a gente conversa.
