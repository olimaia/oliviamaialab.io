---
title: escrever
date: 2020-05-10 11:48:21
---


deslizar os dedos pelo teclado.

mas é que não sei se tenho muita vontade de me dizer escritora. escrever hoje em dia parece um troço meio estúpido. uma espécie de propaganda. como se escrever fosse, sempre, publicidade.

ou porque não basta escrever.

alguma vez bastou?

não estou aqui pra vender nada, você me desculpe.

ou contar histórias? existem ainda histórias pra se contar? serve contar histórias se não há ninguém escutando? precisa tanto malabarismo pra que meia dúzia esteja prestando atenção?

talvez eu deva ser outra coisa. fazer zines, livrinhos caseiros porque as palavras. porque eu não vou me livrar das palavras, não tão cedo. não me parece possível. mas não _escritora_.

a escritora saiu da sala.
