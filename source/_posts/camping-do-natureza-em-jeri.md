---
title: " camping do Natureza em Jeri"
date: 2013-09-08 20:30:11
---

quando chegamos em Jericoacoara, rodamos um pouco e acabamos escolhendo o camping Jeri, no começo da rua São Francisco. o lugar pareceu legal, ainda que um pouco abandonado, mas tinha um quartinho decente por um preço bom. também estávamos um tanto cansados da viagem. no fim do dia percebemos que o lugar era péssimo. o dono passava o dia enchendo a cara e depois vinha com uns papos de bêbado na nossa janela. o espaço estava era MUITO abandonado. achamos melhor fugir de lá o mais rápido possível e tentar o camping do Natureza, com quem eu havia conversado antes pelo telefone. 

o camping fica meio longe do agito, na rua da Igreja, mais ao norte, pertinho da praia da Malhada. mas o espaço pareceu bem mais simpático. também o Natureza (ele é paisagista) nos recebeu muito bem e fez a diária com um preço camarada para ficarmos num dos quartos: 15 reais por pessoa. só teríamos que sair no dia 2 de manhã porque o quarto já estava reservado. 

a turma também era bem legal: muitos argentinos, um colombiano, um mineiro mochileiro trabalhando pelos litorais, uma paulistana que morava na Chapada Diamantina, um de Pedrinhas Paulista que estava na pilha de morar e trabalhar um tempo por ali, um piauense que trabalhava com vídeos numa pilha de revolucionar o marketing em Jeri (entre outras coisas), um guia dos Lençóis Maranhenses que estava ali meio por engano. o clima estava tão bom que quando o Natureza nos ofereceu um desconto para continuar no camping em barraca depois do dia 2, só precisamos fazer as contas para ver se o dinheiro ia ser suficiente para aceitar. 

gostei de lá e recomendo. o Fabio vivia batendo a cabeça nos galhos dos cajueiros que fazem sombra na maior parte do espaço, mas até aí eu também foi atacada pelos galhos algumas vezes. pelo menos tem sombra. sombra no Ceará é um troço muito importante. o preço normal do camping é 20 reais por pessoa nos quartos e 10 reais por pessoa em barraca. para os quatro dias do ano novo, quando Jeri fica toda lotada, o Natureza cobra 400 reais por pessoa. e o camping fica cheio. vai vendo.
