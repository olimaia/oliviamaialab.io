---
title: " bolívia, dia nove: isla del sol"
date: 2011-07-25 15:54:44
---

sem sinal tigo. ergo, escrevo na noite do domingo, mas certo que o post só chega na segunda, quando eu chegar de volta a copacabana pra tomar o ônibus pra la paz.

estou muito perdidamente apaixonada por esse lugar. ainda achamos, aqui na parte norte, que tem menos estrutura que a parte sul, um hostal mui simpático e tudo.

e andamos. andamos que o rogério queria me matar tanto que andamos. tomamos a trilha que percorre toda a ilha até a parte sul e de volta. vimos ruínas incas; uma cidade labirinto chamada chinkana. depois trilha adiante: la ruta sagrada de la eternidad del sol (willka thaki). haja sol e subida e pedra. e a vista do lago titicaca brilhando com o sol forte. por todo lado.

andamos a tarde toda. dia caindo e cinco horas de caminhada voltamos à parte norte. uma bandinha e uma gente vestida de colorido estava dançando porque uma fiesta a santiago.

fui ver o reflexo do pôr do sol nuns picos nevados ao norte/leste. não sei o que são, mas chuto que um deles seja o chacaltaya. uma chola tinha levado um carrinho de mão com roupas e panos pra lavar no lago. veio depois um moleque pra ajudar.

vi um pouco da fiesta, mas estava que não me aguentava em pé. voltei pra um banho rollercoaster quente frio quente frio quente e descansar um pouco. mal sinto minhas pernas.

ainda encontrei forças pra, depois do jantar de sopa de verduras e sanduíche de ovo com tomate, olhar um pouco as estrelas na praia aqui do lado.

afora o turismo, são comunidades paradas no tempo. pro que há de bom e de ruim. porco e galinha e ovelha e burros por todos os lados. vi um monte de menino passando com um rádio enorme, portátil, ouvindo o jogo. nem sei quem ganhou a copa américa, aliás.

mas, sim, muito perdidamente apaixonada por esse lugar. as casas de pedra parecem de mentira. os relevos e esse lago tão estupidamente azul.
