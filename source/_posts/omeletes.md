---
title: " omeletes"
date: 2010-04-18 02:41:13
---

sempre começar, assustador. os cartõezinhos presos por um elástico e incompletos. mais da metade da trama no limbo, esperando sua vez. organização pela metade. medo do majestoso: projeto mais ambicioso o segundo policial com o Pedro Rodriguez, porque duas histórias paralelas que se encontram aqui e ali. porque o quero grande e o sei grande. única possibilidade. então esse começar. assustador. mais certo seria passar pela fase mais braçal de organização e partir para o escrever. mas, mas, mas.

outro: projeto mirabolante fantasioso gigantesco. uma jornada épica em um planeta remoto. magos, monstros. menos a trama confusa e mais o encadeamento das ações, os diálogos. história pra adolescentes, sim, e os adultos interessados. aí que escrevi um primeiro capítulo duvidoso.

enquanto isso a Marcela, _a vida secreta das nuvens_. rumo à página 50. a linguagem às vezes me cansa, mas também me carrega. não sei. sigo. uma revisão e depois resolvo incoerências.

uma coisa de cada vez? queria escrever tudo de uma vez só. quanto mais leio, mais quero escrever. parece que o mundo segue fugindo. o tempo dá uma pirueta no ar e escapa.

poft.
