---
title: " bolívia, dia doze: la paz"
date: 2011-07-28 00:22:39
---

ya! hoje deu pra andar bastante, sentir frio e morrer de calor num espaço de dez minutos etc.

de manhã seguimos pra rua de turista, calle sagarnaga, que é uma ladeirona íngreme e estreita e meu deos que ideia de jerico fazer a rua turística de la paz numa ladeira. que é pra matar o turista, ya? compramos o tour pro chacaltaya e seguimos rua acima porque o comércio ainda todo fechado. depois tomamos uma direita pra seguir um pouco em linha reta acho que av. santa cruz. um caos de carros e gente e carros e carros. atravessar a rua sempre uma aventura. mais pro fim uma feira de rua de roupas de frio (avançando pela rua por que não) e umas bancas que vendiam fetos ressacados de lhama. ui.

voltar voltar. o comércio de artesanato boliviano genérico já estava aberto e compramos ponchos. oba oba.

depois segunda missão do dia foi caçar o mirador killi killi um pouquito mais pro norte e como bom mirador um tanto mais arriba arriba. haja escada e ladeira rogério querendo me matar. la paz é uma cumbuca. montanhas pra todo lado e a cidade pendurada subindo com elas. e o chacaltaya onipresente ali ao fundo, sempre.

depois caçar almoço sem pollo e sem fritura, o que fica cada vez mais difícil. por todo lado é esse cheiro de frango frito e pimentão. até mesmo agora no hostal que esses franceses do quarto do lado ocupando a cozinha só devem saber fazer frango frito socorro.

e descansar. altitude não é brincadeira. estar aclimatado é conceito relativo, ya? um ataque de risos e já fiquei meio enjoada e com falta de ar. deos.

última missão do dia encontrar uma livraria pras encomendas (sim, maurício! o teófilo não achei em lugar nenhum, mas descolei uns dicionários bonzinhos e ainda descobri o que quer dizer INTI, que estava por todo lado em copacabana e na isla del sol) e um livrinho pra mim pra me empurrar nos meus planos de aprender direito o espanhol.

paramos num café pra outra refeição sem pollo e sentar perto da janela ver os adolescentes saídos da escola e o rush paceňo (rush paulistano é nada) nessas ruas que deveria passar um carro mas passam dois a dez ou vinte centímetros de distância entre eles e colados na calçada, ya? penso centro de são paulo. com uma inclinação de 45 graus. pense rua pra carroça passando dois toyotas lado a lado loucamente.

agora sentar aqui na sala de televisão e ver jornal alheio comercial alheio (esse aqui que o cara chega no céu depois de morrer dizendo que trabalhou, cuidou da saúde, estudou etc; opa, mas não pagou os impostos, vai direto pro inferno! really). e depois a novela mexicana triunfo del amor. que esse povo chora que é uma beleza.

que amanhã vou acordar cedo pra fazer a brasileira louca que nunca viu neve (vá, aquele pedacinho em uyuni quase não conta).

buenas!
