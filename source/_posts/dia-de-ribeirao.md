---
title: " dia de ribeirão"
date: 2013-09-27 22:20:19
---

![img_6476](/img/posts/img_6476.jpg) hoje depois do almoço a Roberta, nossa anfitriã, nos levou por uma trilha curta até o Ribeirão do Meio, nos limites do parque. a gente ia mais cedo mas o sol demorou para aparecer. no ribeirão a água estava bem gelada como convém às águas aqui da região, embora bem mais quente do que a maioria dos rios que cruzam o parque. vai vendo. aí tomar coragem e mergulhar (e eu demoro para tomar coragem; nisso o Fabio é bem mais prático). o sol quente serve de incentivo.

![deu para subir na pedra ali ao fundo e descer escorregando.](/img/posts/img_6436.jpg)

deu para subir na pedra ali ao fundo e descer escorregando. MUITO legal.

![Roberta e eu.](/img/posts/img_6443.jpg) 
Roberta e eu.

![img_6450](/img/posts/img_6450.jpg) 
o escorregador ao fundo.
