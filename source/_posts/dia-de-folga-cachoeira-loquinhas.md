---
title: " dia de folga: cachoeiras Loquinhas"
date: 2013-10-27 18:24:22
---

na quarta-feira fiquei com o dia de folga (primeiro de uma folga dupla depois de trabalhar dobrado na terça-feira) e aproveitei para conhecer as cachoeiras das Loquinhas, que são mais perto da cidade de Alto Paraíso: uma caminhada de 5 km e segundo diziam quase sempre debaixo do sol. pensei que de qualquer forma seria bom chegar com o sol alto, porque geralmente cachoeira se esconde na sombra quando o sol vai baixando. saí umas 10 horas, passei para ver o Fabio trabalhador na Eco Nois e segui pelo caminho que me indicaram. logo topei com uma placa que indicava mais 3 km de caminhada. não tinha caminhado nem dez minutos. 

vai saber a partir de onde fizeram a medição para os 5 km, mas ok. eram umas 10h15 quando comecei a caminhar. depois de um terço do caminho passou um carro e pensei em pedir carona, mas depois pensei que mais 2 km, eu posso ir andando, não vai ser o fim do mundo. claro que pensei isso antes de começar a subida. capoft. fato é que cheguei em menos de uma hora na Fazenda Loquinhas. o acesso à trilha e aos poços custa 15 reais. 

a trilha é uma passarela de madeira suspensa no meio do mato. à esquerda vão surgindo as entradas para os poços. logo no segundo encontrei a moça que tinha passado de carro. ela também pensou em me oferecer uma carona. conclusão: duas lentas.

a água é assim mesmo azulzinha-verdinha transparente. mesmo quando o sol se esconde atrás das copas das árvores ou de alguma nuvem; só vira um verde mais escuro. um troço fora do comum. a água bem gelada como convém. e como falta chuva, os poços estão todos bem rasos. não dá para entrar de uma vez com um mergulho. não sei entrar em água gelada se não for para entrar de uma vez. urf. o tempo passa num ritmo diferente num lugar desses. 

depois encontrei outra vez noutro poço a moça do carro. ela era de Brasília e ficamos um tempão conversando sobre literatura enquanto eu comia um salgadinho porcaria. visitamos ainda os últimos dois poços, que estavam com a água muito parada e barrenta por causa da falta de chuva. não devia ser cinco horas quando começamos a voltar. aí ganhei uma carona para Alto Paraíso. quase uma semana depois de chegar pareceu que estava finalmente começando a conhecer alguma coisa da Chapada dos Veadeiros.
