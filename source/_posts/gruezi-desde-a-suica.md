---
title: " grüezi desde a Suíça"
date: 2015-06-28 06:22:10
---

hoi. dia 17 de junho pousei em Amsterdã, me deram um carimbo no passaporte e tomei outro avião rumo ao _flughafen_ de Zurique, na Suíça. 

aqui é tudo muito bonitinho e organizado, como era de se esperar, e as casinhas são todas iguais nos arredores da cidade, com os telhados da mesma cor e janelas no mesmo estilo, as árvores e plantas muito verdes nesse começo de verão etc. as pessoas também são muito suíças, com seu suíço-alemão incompreensível, seus relógios e trens que partem pontualmente às 16h43.  dá muito fácil pra se acostumar a viver por aqui, se você nunca mais pensar no que era a vida em outros lugares, em como é a vida no terceiro mundo, em como tudo isso só pode existir porque existe o tal do terceiro mundo produzindo seus aparelhos eletrônicos comprados com 50% de desconto.
