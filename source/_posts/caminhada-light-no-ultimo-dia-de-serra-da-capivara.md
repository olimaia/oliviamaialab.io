---
title: " caminhada light no último dia de Serra da Capivara"
date: 2013-09-24 19:30:55
---

no sábado fizemos nosso terceiro e último dia de parque. depois da sexta a gente estava bem mortinho, e o Fabio estava preocupado com essas escadas sem noção que o povo inventa nos lugares os mais improváveis. o Leandro prometeu um dia light: Alto da Pedra Furada e vista para o Baixão das Mulheres, exposição da megafauna no centro de visitantes, Boqueirão da Pedra Furada e Sítio do Meio, com algumas vistas panorâmicas mais tranquilas pelo caminho. caminhamos mais foi na primeira parte do dia. começamos com uma subida violenta e uma escada. ops. 

aí trilha até o Baixão das Mulheres e voltar. dessa vez encontramos no caminho um pedaço de casco de tatu que a onça devia ter comido. 

almoço foi no centro de visitantes depois de ver os ossos dos animais da megafauna. unha da preguiça gigante é do tamanho da minha mão.

depois disso foi só passear mesmo. o sol é que não dava muita trégua e sombra nessa época do ano só nos paredões. ver uns sítios maiores e mais conhecidos, uma vista panorâmica do Baixão da Pedra Furada (subidinha leve) e depois de carro até o Sítio do Meio. dava para levar a tia velha nessa segunda parte do dia. tem mais fotos no álbum do dia, que já está publicado aqui no blog. a gente que estava cansado ainda do dia anterior. ficamos depois capotados no hotel. conhecemos o Flávio e a Tatiana que também estavam visitando o parque (na verdade a gente tinha topado com eles num café da manhã e depois na Serra Vermelha) e fomos dividir uma pizza (a do pizzaiolo que tinha trabalhado 18 anos em São Paulo).
