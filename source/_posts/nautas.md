---
title: " nautas"
tags:
  - cortázar
  - escrever
url: 3214.html
id: 3214
categories:
  - blog
date: 2011-07-11 12:24:47
---

porque como um avião prestes a decolar que no entanto ainda mantém por alguns segundos as rodas traseiras no solo antes de um impulso final e obedece à reta da pista de voo, agarro-me mais um pouco ao meu mestre cortázar e faço que sim, sim, essa palavra e depois essa, bueno. ensaiando aqui e ali alguns voos independentes sem nunca me esquecer dessa pista de voo, para a qual sempre volto, quando me falta essa clareza de caminho que algumas decolagens pedem.
