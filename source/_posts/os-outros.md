---
title: " os outros"
date: 2009-08-29 07:06:07
---

> [...] Deixava então que todas as pessoas passassem por ele, e permanecia quieto. Mas sentia certo desprezo por todas elas. Secretamente suspeitava de que cada pessoa com quem falava era culpada das piores coisas. Além disso, acreditava não ver nelas sinal de vergonha. Não pensava que sofressem como ele. Parecia que lhes faltava a coroa de espinhos de seus próprios remorsos.

_O Jovem Törless_, Robert Musil [trad. Lya Luft]
