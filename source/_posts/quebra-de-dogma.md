---
title: " quebra de dogma"
date: 2009-02-07 19:49:34
---

aí eu conto pra um amigo a história da sala de aula na caixa de sapatos, sempre pra mim sintomática de uma condição limitante de criatividade, e que talvez na verdade eu não tenha nenhuma imaginação. e ele, mui eloquente, sem trema: "que idiotice. essa professora é uma imbecil." então, ok. desmoronam as muralhas, caem impérios, etc.
