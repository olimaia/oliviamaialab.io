---
title: " os sintomas"
date: 2012-12-09 17:00:47
---

atropelando a causa mas se enumeram os sintomas: [i'm going crazy in my job](http://www.salon.com/2012/10/18/im_going_crazy_in_my_job/).

> you feel like you don’t belong there _because you don’t belong there._

and yet. trabalhamos para juntar dinheiro para comprar um apartamento e um carro e roupas e coisas e depois um carro melhor uma televisão melhor para contas maiores e trabalhar mais ainda porque a aposentadoria etc. aí você precisa trabalhar com algo que você goste, porque afinal vai viver para o trabalho e nada mais lógico que viva para algo que goste. outro dia no colégio ouvi a conversa de uma babá ao telefone com a filha dela e depois com outra babá, e entendi que trabalhava de babá e deixava a filha com uma babá, e pensei o quão bizarro era pensar que essa última babá deixasse também o filho dela com outra pessoa, ou talvez numa creche, e que nessa creche talvez trabalhassem pessoas que precisassem deixar os filhos com ainda outras pessoas, e assim por diante etc. e então trabalhar mais, porque afinal é necessário pagar os cuidados alheios com seus filhos e vai aí mais uma conta no fim do mês. quão bizarro pode ser essa economia que se autoalimenta feito uma cobra que devora o próprio rabo. aí o infeliz que não quer trabalhar é preguiçoso e imaturo: bem vindo à vida dos adultos, fecha bem os olhos que talvez você não sinta nada. anota aí o início do meu abandono da vida adulta.
