---
title: " sobre a seriedade"
date: 2017-01-20 17:32:18
---

> I think seriousness and humor are equally important, which is why I eagerly await the day when seriousness will get its comeuppance and start envying humor. Humor, for example, comes in many varieties, while seriousness is never organized by categories, although it clearly should be. Dear Critics, since you employ the term “absurd humor,” you should introduce its counterpart, “absurd seriousness.” Learn to distinguish between forced and primitive seriousness, lighthearted and gallows seriousness. This bracingly sensical conception will jump-start critics and journalists alike. Do we not require, in life as in art, indiscriminate seriousness? bawdy seriousness? sparkling seriousness? spirited seriousness? I would read with pleasure about thinker X’s “terrific sense of seriousness,” about bard Ys “pearls of seriousness,” about avant-garde Z’s “offensive seriousness.” Some reviewer or other will finally decide to remark that “playwright N. N.’s feeble play is redeemed by the effervescent seriousness of its conclusion” or that “in W. S.’s poetry one catches notes of unintentional seriousness.” And why don’t humor magazines have columns of seriousness? And why, moreover, do we have so many humor magazines and so few serious ones? Well?

Wislawa Szymborska, _Nonrequired Reading_
