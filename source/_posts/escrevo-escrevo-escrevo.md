---
title: " escrevo escrevo escrevo"
date: 2009-11-13 03:30:06
---

escrevo escrevo escrevo e escrevi duas páginas. escrevo escrevo escrevo escrevo e o livro ainda 37 páginas.

meu amigo Rogério: e aí, como está indo o livro?

eu: muito bem, é o melhor livro que já escrevi na minha vida.

ele: já chegou na página 40?

      ...

e não tanto por economia de palavras ou adjetivos, talvez mais porque escrevo escrevo e reescrevo e ali há um belo parágrafo, e gosto dele, e volto nele, e escrevo mais um pouco dele. um belo parágrafo, sim, mas daí nem meia página.

a princípio isso não me incomoda tanto, senão pela constatação que escrevi um monte e na verdade uma página e meia. por ora, sobressai a sensação que são mesmo belos parágrafos, e gosto deles, e que seja afinal um livro de 60 ou 80 páginas, porque continuando assim além de tudo pode ser também um belo livro.
