---
title: " ao que poderia existir como verdade"
date: 2008-06-19 02:37:35
---

parece certo que o problema de termos como "real" ou "verdade" (e afins) está no próprio termo. que é essa insistência humana em inventar nome para conceitos abstratos ou essencialmente subjetivos, quase sempre inexistentes.
