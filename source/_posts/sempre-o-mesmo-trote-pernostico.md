---
title: " sempre o mesmo trote pernóstico"
date: 2010-03-23 21:49:41
---

> Não pretendo ler mais Machado de Assis, a não ser seus afamados contos. Talvez também o começo de _Dom Casmurro_, do qual já li crítica que me despertou a curiosidade. (...) Por vários motivos: acho-o antipático de estilo, cheio de atitudes para 'embasbacar o indígena'; lança mão de artifícios baratos, querendo forçar a nota da originalidade; anda sempre no mesmo trote pernóstico, o que torna tediosa a leitura. Quanto às ideias, nada mais do que uma desoladora dissecação do egoísmo e, o que é pior, da mais desprezível forma do egoísmo: o egoísmo dos introvertidos inteligentes.

_Os cadernos do cônsul Guimarães Rosa_, trecho dos diários do escritor citado em _Valor Econômico_, 14/01/2002.
