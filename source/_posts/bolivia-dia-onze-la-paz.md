---
title: " bolívia, dia onze: la paz"
date: 2011-07-27 02:36:32
---

la paz é o caos. o caos em forma de uma ladeira gigante. imagino quem chega na bolívia direto em la paz o que não sofre. os engraxates que trabalham na rua usam máscara cobrindo todo o rosto, ou um gorro com abertura nas olhos, porque a fumaceira. as cholas com barraquinhas vendendo bolachas e afins, tão comuns nas outras cidades por que passamos, parecem aqui terrivelmente deslocadas. as feições estereótipo de boliviano ainda estão presentes, ainda que mais diluídas. a ponto de um taxista levar um tempo pra sacar que a gente não era daqui, e depois elogiar o espanhol do rogério.

por todo lado há filas pro cadastro biométrico pras eleições. a porta aqui do hostal tem uma fila que segue até fazer volta no quarteirão. parece que amanhã é o último dia e segundo o telejornal o sistema dá pau toda hora e o povo está ficando bem bravo.

depois na sala de televisão novela mexicana de uns três anos atrás.  deos. amanhã andar mais, que agora sim estou melhor.
