---
title: " olivia na patagônia"
date: 2015-01-19 17:28:39
---

esse teclado suíço-alemao nao tem til entao vamos gringando. (também é um teclado QWERTZ o que significa que o Z e o Y estao trocados além de todos os acentos estarem nos lugares errados.) que é por um breve _status update_: estou em San Martín de los Andes, o _outro_ point de esqui da Argentina, que obviamente no verao é um point de balneários no lago e gente com grana desfilando pelas ruas. 

encontrei Oliver em Uspallata depois de sair de Mendoza e de lá tocamos a Malargüe; depois uma looonga viagem a Copahue passando pela cidade de Neuquén pra comprar uma barraca e colchonetes pra dormir na barraca; desde aí estamos em camping: Copahue, Villa Pehuenia, San Martin de los Andes. nao temos gás pra cozinhar e cozinhamos com lenha e pedras e qualquer improvisaçao de grelha pra apoiar a panelinha. fizemos até um RISOTO.

estou aprendendo a usar esse computador com esse teclado torto e instalando as coisas etc. ainda tem muito pra fazer e muito backup pra restaurar, entao preciso de um pouco mais de paciência. sem contar que estou aqui curtindo a companhia etc. a newsletter deve sair em breve que eu comecei a escrever a mao e preciso passar pro computador. também as fotos dos últimos lugares que tem bastante desde Mendoza com minha mae fazendo rappel, o Aconcagua e tudo mais. pra seguir acompanhando em tempo (quase) real é só ficar de olho nas fotos que vou tirando com o celular.
