---
title: " leveza"
date: 2010-03-04 18:38:38
---

e escrita do Enrique Vila-Matas tem uma leveza. quando tudo conspira para o peso, para o terrível. quando mesmo as palavras deveriam ceder ao peso do significado e do tempo. mas as palavras flutuam.
