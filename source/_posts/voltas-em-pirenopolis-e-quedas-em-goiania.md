---
title: " voltas em Pirenópolis e quedas em Goiânia"
date: 2013-12-20 11:23:12
---

aí que na terça-feira fui com um guia amigo do dono do hostel até a cachoeira Bonsucesso, que na verdade são seis cachoeiras na fazenda Bonsucesso. a água estava meio suja por causa da chuva (quer dizer, suja de barro, que não é bem sujeira). foi uma caminhada tranquila de cinco quilômetros e mais uns metros dentro da fazenda. deu para mergulhar. saiu até sol!

na volta rolou uma carona com o povo da fazenda até o carro do guia e depois seguimos para o IPEC. estava fechado para visitação, mas o portão estava aberto e fomos entrando (ops). lá dentro o guia conversou com um rapaz que disse que a gente podia dar uma volta pela área aberta e tudo bem. as bioconstruções são muito legais e sempre coloridas. pena que não deu pra fazer um tour de verdade, com explicações e aquela coisa toda.

ói aqui o álbum de fotos. Pirenópolis é uma cidade boa de visitar, mais bonita do que Alto Paraíso, mas não sei se mais simpática. fiquei pouco tempo e acho que precisaria pelo menos uma semana pra ter uma opinião mais precisa. saí na quarta-feira de manhã num ônibus mei sem vergonha da viação Goianésia rumo à Goiânia. a rodoviária de Goiânia é um shopping e grande demais pros meus 15kg nas costas. descendo as escadas da praça de alimentação perdi o degrau e torci o pé; só não rolei escada abaixo porque já estava nos últimos degraus mesmo. levei foi um tombão mei torto. duas pessoas me arrumaram gelo e troquei meu tempo de almoço por vinte minutos segurando gelo no tornozelo. capow. aí os 15kg viraram uns 30 mas devagar consegui chegar no embarque. comprei um lanche sem vergonha e um pão de mel para comer no caminho. 13h30 saí num ônibus todo moderninho da Nacional Expresso com destino a Uberaba, onde mora minha prima querida. (a ironia foi o médico recém-formado fazendo residência em ortopedia que foi conversando comigo boa parte do caminho.)

aí estava praticamente em casa, com direito a prima me buscando de carro na rodoviária. tanto que agora estou me sentindo até mei resfriada. domingo vamos conhecer os dinossauros de Uberaba (aguardem fotos) e segunda-feira pegamos estrada rumo a São Paulo, com uma parada estratégica em Atibaia para deixar os animais (uma pitbull, uma salsicha e dois gatos insanos) na casa da minha tia.
