---
title: " e porque depois da curva está o fim de semana"
date: 2014-04-03 09:43:20
---

pare tudo o que estiver fazendo e [perca alguns minutos neste link](http://www.buzzfeed.com/spenceralthouse/the-definitive-ranking-of-robins-exclamations-from-batma). ou, se tiver mais do que alguns minutos, [leia esse texto do London Review of Books](http://www.lrb.co.uk/v36/n04/james-wood/on-not-going-home) sobre _não voltar pra casa_. e se puder dá uma visitada no álbum de fotos dos meus dias no Retiro Tao Tien, que publiquei no autonautas. também escrevi sobre minha chegada em Amparo e Monte Alegre do Sul. e também um pouco de Liniers: 

![1861033](/img/posts/1861033.jpg) por Liniers
