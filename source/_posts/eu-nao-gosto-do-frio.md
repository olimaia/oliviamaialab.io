---
title: " eu não gosto do frio"
date: 2009-07-15 23:40:33
---

está decidido. acabou com essa frescura de detestar o calor no verão e o frio no inverno. eu não gosto mesmo é do frio. o calor pode ser até chato, em excesso, em São Paulo. mas o frio é pior. e olhar pro céu e tudo branco, e não saber onde está o sol, onde por um acaso estaria o sol, e de repente anoiteceu.
