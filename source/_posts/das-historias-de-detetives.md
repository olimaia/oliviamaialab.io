---
title: " das histórias de detetives"
date: 2008-05-21 02:25:00
---

> (...) in my case at least, detective stories have nothing to do with works of art. It is possible, however, that an analysis of the detective story, _i.e._, of the kind of detective story I enjoy, may throw light, not only on its magical function, but also, by contrast, on the function of art.

depois de "The simple art of murder", do Raymond Chandler (e de ele falar mal das histórias clássicas de detetive), um pouco do outro lado em "[The guilty vicarage: notes on the detective story, by an addict](http://harpers.org/archive/1948/05/0033206#)", por W. H. Auden (texto de 1948).
