---
title: escrever e ler
date: 2021-10-05 08:53:47
---

ou:
como não escrever

porque escrever e ler são parte de uma mesma atividade. não existe escrita sem leitura.

a vida --

a vida não é suficiente.

a vida se repete.

a vida, as horas.

a vida se arrasta nos minutos e a escrita murcha e se esvazia e sobra uma vontade de golpear o teclado.

vazia.

voltar, então. voltar à leitura, voltar à literatura. não existe escrita sem leitura.
