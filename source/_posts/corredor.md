---
title: " corredor"
date: 2011-05-05 14:23:00
---

> Unilateralidade, não única do homem. Sente-se que viver significa projetar-se num sentido (e o tempo é a objetivação dessa linha única). Não podemos senão avançar por um corredor onde as janelas ou as grades são o incidental naquilo que realmente importa: a marcha para um extremo que (já que o corredor somos nós mesmos) nos vai distanciando cada vez mais do ponto de partida, das etapas intermediárias... É escuro e não sei como dizer: sentir que minha vida e eu somos duas coisas, e que se fosse possível tirar a vida como se tira o casaco, pendurá-la por algum tempo no encosto da cadeira, seria necessário saltar planos, escapar da projeção uniforme e contínua. Depois vesti-la de novo, ou procurar outra. É muito _chato_ que só tenhamos uma vida, ou que a vida tenha apenas um modo de acontecer. Por mais que possamos preenchê-la de fatos, embelezá-la com um destino bem planejado e realizado, _o molde é um só_: quinze anos, vinte e cinco, quarenta -- o corredor. Levamos a vida como levamos os nossos olhos, do modo que nos _modela_; os olhos veem o futuro do espaço, assim como a vida é sempre a dianteira do tempo. Hilozoísmo, ansiedade do homem para viver carangueijo, viver pedra, ver-do-alto-de-uma-palmeira. Por isso o poeta se _aliena_.

Julio Cortázar, _Diário de Andrés Fava_
