---
title: " aquisições paperback"
date: 2010-05-25 19:13:06
---

depois de ter lido sobre _The Turn of the Screw_, do Henry James, em _O livro porvir_, do Blanchot, e com o reforço de uma indicação do amigo [Ron](http://www.skoob.com.br/usuario/mostrar/14033), comprei uma edição de sete reais. e pra aproveitar o frete também uma edição de quatro reais do _Bartleby_, do Melville, que já há um tempo tenho alguma curiosidade pra ler. e depois que li esses dias o _Billy Budd_, e aproveitar o pique Melville, quem sabe ler de uma vez o _Moby Dick_, que afinal já tenho aqui (dei de presente pro marido treinar o inglês).

também porque agora estou aproveitando essa paz de tempo pra conseguir ler tudo o que nunca tinha tempo de ler. mesmo que a concentração ainda não esteja de todo muito favorável, está melhor do que quando faculdade trabalho escrita socorro. agora só trabalho escrita projeto de mestrado socorro. pensando bem, não, não está muito melhor.

vai entender.

mas é que esses livrinhos edição paperback todos baratinhos e simpáticos me encantam demais, e não resisti me gabar um pouquinho. no mais, vou fazer uma pausa nesse momento comprar livros loucamente (comprei uns dez livros esse mês, mais da metade em inglês, porque são tão terrivelmente mais baratos) e me ocupar de ler o que tenho aqui pra ser lido.
