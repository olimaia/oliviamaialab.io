---
title: " neste ano que começa olhe mais pra cima"
date: 2016-01-01 19:00:00
---

![img_2250](/img/posts/img_2250.jpg)

sério: olhe mais pro céu. pare e mova um pouco a cabeça, levante um pouco o queixo. olhe pra cima. desenhe na memória a linha dos edifícios; repare naquela antena parabólica, no pássaro enfiado entre as folhas da árvore, nas pombas enfileiradas na fiação. 

![img_0657](/img/posts/img_0657.jpg)

caminhamos sempre olhando pro chão. claro: senão periga tropeçar. ninguém quer tropeçar em plena avenida. sem contar que precisa desviar dos postes; os postes estão por todos os lados. aqui em Lençóis o calçamento das ruas é todo irregular; meu filho, tem que olhar por onde anda senão você vai com a cara nos paralelepípedos. e esses calombos de cimento da Embasa, de quando instalaram a rede de esgoto, espalhados por todo lado. melhor medir bem os próprios passos. mas de vez em quando: pare. 

![img_3201](/img/posts/img_3201.jpg)

olhe pra cima. não digo olhar na direção do horizonte, botar a cara na janela do prédio e de cima inspecionar a cidade lá embaixo e as nuvens na distância. estou dizendo: ponha-se num lugar à céu aberto e olhe pra cima. olhe acima dessa linha de cabeças humanas que se movimentam ao seu redor. como quem entra numa igreja antiquíssima e aprecia os afrescos desbotados. 

![img_2573](/img/posts/img_2573.jpg)

acredite em mim: o mundo de repente fica maior. o mundo fica gigantesco. olhar pra cima é sair um pouco de dentro da própria cabeça, dos próprios problemas. olha essa nuvem, esses desenhos. essa cor cinzenta anunciando uma tempestade. deixa entrar a luz. faça isso como um lembrete de que está tudo bem, vai ficar tudo bem. o mundo é tão grande. você é parte de tudo isso. vai dar tudo certo. 

![img_1963](/img/posts/img_1963.jpg)

não precisa se preocupar: o mundo ainda vai estar ali quando você abaixar o olhar.
