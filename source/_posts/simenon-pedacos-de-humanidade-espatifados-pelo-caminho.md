---
title: " Simenon: pedaços de humanidade espatifados pelo caminho"
date: 2010-05-01 04:14:25
---

o verbete da Wikipedia (em inglês) sobre Simenon diz que ele foi um escritor prolífico.

ok.

só com Maigret ele escreveu 75 livros e 28 contos. mas parece que ele escreveu quase 200 romances, mais de 150 novelas e um monte de outra coisa. imagina escrever 80 páginas por dia.

quando eu crescer quero ser igual ele.

recentemente li "[O homem que via o trem passar](http://www.bondfaro.com.br/preco--books--o-homem-que-via-o-trem-passar-colecao-40-anos-40-livros-georges-simenon-8520918611.html)", um não-policial, sem Maigret. achei num sebo, no topo da estante. o estilo é o mesmo. cru, conciso, sem muita descrição, mas ainda assim se prendendo num detalhismo quase paradoxal. eu diria: é um livro terrível. no melhor dos sentidos.

com Maigret, li uns 10. sem dúvida um dos meus policiais favoritos. acho curiosa essa mistura de um policial clássico com os hard boiled americanos. é uma leveza falsa que se dá ao crime. propositalmente falsa. não há nada de leve em um assassinato. mas Simenon não vai esfregar isso na sua cara. porque os assassinos são, quase sempre, tão gente quando o próprio Maigret.

talvez seja isso: mesmo quando o desfecho não leva à prisão do assassino você não fica com essa sensação de que a justiça não foi feita. pior ainda, fica a sensação de que a justiça não tem nada a ver com isso. quem pode julgar?

um dos meus favoritos foi um dos últimos que li: "[O enforcado](http://www.bondfaro.com.br/preco--livros--o-enforcado-maigret-simenon-8525413305.html)". há uma investigação, há vítimas (e também um suicídio aparentemente absurdo), mas no fundo o que resta é um terror ainda maior: nossa humanidade. e aí, então, diria uma personagem minha, eis que mais um pedaço de humanidade espatifado na avenida. e não há justiça que possa fazer qualquer coisa a respeito disso.
