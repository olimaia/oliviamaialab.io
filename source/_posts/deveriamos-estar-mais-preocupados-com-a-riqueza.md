---
title: " deveríamos estar mais preocupados com o problema da riqueza"
date: 2016-01-08 16:09:00
---

> Asking questions about the rich has been portrayed since the dawn of wealth as envy; asking questions about the poor is considered practical and sympathetic, moral and problem-solving. But no problem can be solved while political institutions won’t recognise that poverty has a cause.

([via](http://www.theguardian.com/commentisfree/2015/oct/19/un-poor-wealth-sustainable-development-goals?CMP=fb_gu).)

ou: resolver o problema da pobreza como quem cobre buracos numa estrada com asfalto de má qualidade por onde passam diariamente centenas de caminhões pesados jamais vai resolver coisa alguma. a pobreza não é uma doença a ser _erradicada_. assim como a violência nas cidades grandes não é um vírus que precisa ser destruído; a pobreza tem uma causa, e se não examinarmos a _causa_ do problema, não será um bilionário bem-intencionado que solucionará os infortúnios de quem nasceu no país, cidade, bairro e família _errados._

> These basic assumptions – poverty is the problem, growth is the answer, climate change can be tackled separately to consumption, and corporate behaviour is neither here nor there – extend far beyond the UN, into political cultures everywhere.

se você lê inglês, _[leia o artigo](http://www.theguardian.com/commentisfree/2015/oct/19/un-poor-wealth-sustainable-development-goals?CMP=fb_gu)_. me parece um bom ponto de partida em direção às perguntas certas.
