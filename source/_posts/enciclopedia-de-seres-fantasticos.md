---
title: " enciclopédia de seres fantásticos"
date: 2017-02-03 09:01:45
---

> “The person possessed by hatred. Known since time immemorial. He doesn’t change; only the methods change that he employs in gaining his end. Moderately ominous when he acts in isolation, which, however, rarely occurs, as he is contagious. He spits. He spreads chaos in the conviction that he is creating order. He likes making pronouncements in the first person plural; this may initially be groundless, but it becomes increasingly justified with persistent repetition. He departs from the truth in the name of some higher order. He is devoid of wit, but God save us from his jokes. He is not curious about the world; in particular he does not wish to know those whom he has singled out as enemies, rightfully considering that this might weaken him. As a rule, he sees his brutal actions as being provoked by others. He doesn’t have doubts of his own and doesn’t want the doubts of others. He specializes, either individually or, preferably, en masse, in nationalism, anti-Semitism, fundamentalism, class warfare, generational conflict, and various personal phobias, to which he must give public expression. His skull contains a brain, but this doesn’t discourage him...”

Wislawa Szymborska, _Nonrequired Reading_
