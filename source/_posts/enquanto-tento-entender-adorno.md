---
title: " enquanto tento entender Adorno"
date: 2008-11-18 02:32:41
---

do ensaio:

> Em vez de alcançar algo cientificamente ou criar artisticamente alguma coisa, seus esforços ainda espelham a disponibilidade de quem, como uma criança, não tem vergonha de se entusiasmar com o que os outros já fizeram. (...) Ele não começa com Adão e Eva, mas com aquilo sobre o que deseja falar; diz o que a respeito lhe ocorre e termina onde sente ter chegado ao fim, não onde nada mais resta a dizer: ocupa, desse modo, um lugar entre os despropósitos.

Theodor Adorno, _O ensaio como forma_, no livro "Notas de literatura I"
