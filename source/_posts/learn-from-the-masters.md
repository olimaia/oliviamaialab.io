---
title: " learn from the masters"
date: 2013-02-15 18:39:22
---

> Mark Twain once said, “Show, don’t tell.” This is an incredibly important lesson for writers to remember; never get such a giant head that you feel entitled to throw around obscure phrases like “Show, don’t tell.” Thanks for nothing, Mr. Cryptic.

via [McSweeney’s Internet Tendency: The Ultimate Guide to Writing Better Than You Normally Do.](http://www.mcsweeneys.net/articles/the-ultimate-guide-to-writing-better-than-you-normally-do)
