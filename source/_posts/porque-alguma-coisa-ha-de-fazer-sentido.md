---
title: " porque alguma coisa há de fazer sentido"
date: 2010-03-23 05:34:34
---

eu gosto de escrever. gosto tanto de escrever que gosto até mesmo de escrever relatório de correção (de redações). há qualquer alegria em se organizar a sintaxe e escolher palavras.
