---
title: " da não-existência"
date: 2014-01-09 15:31:41
---

> As coisas que não existem são em número muito maior do que as que chegam a existir. O que nunca existirá é infinito. As sementes que não encontraram sua terra nem sua água e não se transformaram em planta, os seres que não nasceram, os personagens que não foram escritos. (...) O irmão que não existiu porque você existiu no lugar dele. Se o tivessem alguns segundos antes ou alguns segundos depois, você não seria quem é e não saberia que sua existência se perdeu no ar sem que você mesmo se desse conta disso. Aquilo que não chega a ser nunca sabe que poderia ter sido. Os romances são escritos para isso: para compensar no mundo real a ausência perpétua daquilo que nunca existiu.
> 
> Tomás Eloy Martinez, _Purgatório_
