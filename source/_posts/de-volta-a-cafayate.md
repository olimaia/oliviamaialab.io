---
title: "de volta a cafayate"
date: 2014-11-07 23:46:59
---

a _quebrada de las flechas_ é um troço incrível. pra se sentir num episódio de Doctor Who. parecem dunas, pela cor e a cara de areia, mas dunas com formas, como se cuspidas das profundezas da terra em diagonal; e assim ficaram, apontando aos céus. 

alguns quilômetros depois o ônibus sai da quebrada e o cenário volta a ser o vale à esquerda e a estrada estreita adiante. dessa vez não tinha ninguém pra me oferecer hostel na saída do ônibus, que é pra dizer que ninguém espera muito mochileiro chegando de Angastaco pr’aquelas bandas. toquei pro hostel Backpackers onde tinha ficado com a Andressa da outra vez e fui recepcionada pelo famoso Mudo, marido da Emma, a dona que eu tinha conhecido antes. fiquei no mesmo quarto e já me meti em conversa alheia enquanto três meninas discutiam o percurso rumo ao norte do norte: uma argentina, uma mexicana e uma espanhola. ao final acabei arrastando a espanhola, chamada Sílvia, à _casa de la empanada_, que é basicamente uma das melhores empanadas do universo, e tomamos uma cerveja mais ou menos gelada. depois ainda o tiozinho que apareceu pra fazer música ao vivo tentou cantar Samba para olvidar (minha nova música favorita) e falhou miseravelmente porque não sabia a letra (eu também não sei). enfim enfim. _me gusta Cafayate_.
