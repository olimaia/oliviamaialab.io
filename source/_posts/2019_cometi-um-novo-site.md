---
title: "cometi um novo site"
date: "2019-06-10 18:05"
---

ops.

talvez nem pareça que o site mudou, mas a real é que mudou completamente.

não tem mais wordpress. não tem mais banco de dados. ocupa menos de 1/10 do espaço que ocupava antes. mais ou menos.

e estou escrevendo esse post usando um editor de código, o que dá pontos de **#NERD** pra coisa toda.

construi a nova estrutura usando o [hexo](https://hexo.io/pt-br/), que é um gerador de site estático. isso quer dizer que, chans, o site agora é estático. what-you-see-is-what-you-get.

e ainda tem [lojinha](/loja) e downloads gratuitos. quer dizer, tem lojinha; no momento os downloads não estão funcionando porque a empresa que me fornece a internet aqui na roça decidiu que não tem mais IP público pra ninguém. estamos atrás dum NAT que é tipo uma rede doméstica gigante que se comunica com o mundo por um só IP público pra todo mundo. os downloads estavam hospedados no meu próprio servidor aqui em casa, mas esse servidor agora não consegue mais se comunicar com o mundo. ops ops.

calma que vou arrumar um lugarzinho limpinho pra pendurar os downloads e aviso todo mundo.

enquanto isso, pros curiosos, fiz essa página aqui: [src](/src) (porém já preciso atualizar porque por motivos de **#nerd** troquei o gerenciador de janela e **#NERDFELIZ**).
