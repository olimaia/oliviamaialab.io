---
title: " tapioca em Flecheiras"
date: 2013-09-07 16:00:48
---

desde que cheguei no Ceará fiquei uns dias numa vontade de comer tapioca. só no penúltimo dia em Fortaleza antes de seguir para Trairi que fomos numa padaria e matei um pouquinho a vontade. indo para o Guajiru, de carona com a Esther, também paramos no caminho para almoçar tapioca e tomar coco em Paraipaba. em Flecheiras encontramos ali na rua principal uma tapiocaria mui boa. um lugarzinho pequeno pintado de lilás com o cardápio todo na parede. também vende sopas. a dona é uma moça de Fortaleza chamada Silvania. 

nas duas vezes que fomos conversamos bastante com ela. até sugerimos a tapioca de queijo coalho que a gente tinha experimentado na padaria em Fortaleza, com o queijo ralado e misturado na goma da tapioca. ela curtiu a ideia, testou uma versão própria e usou a gente de cobaia para ver se tinha funcionado. ficou bem bom. faltou dar um nome para a (re)invenção. eu e o Fabio não somos lá muito bons para nomes.

o lugar é novo. na última vez que fomos ela estava criando um cardápio para deixar nas mesas. de qualquer forma, até agora uma das melhores tapiocas de queijo coalho que experimentei por aqui.
