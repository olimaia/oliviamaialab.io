---
title: " sobre santa maría e belén"
date: 2014-11-10 23:15:40
---

dois povoados que cresceram demais. belén tem até semáforo. mas a gente é sensacional. te convidam pra almoçar (e você já tinha comido um sanduíche mas ela trouxe o prato de macarrão com queijo ralado e tudo e um copo com suco geladinho e eu almoço duas vezes sem nem pensar duas vezes), te ajudam a chegar nos lugares. alguém em Cafayate comentou sobre essa hospitalidade catamarquenha e eu tive que ver pra acreditar.
