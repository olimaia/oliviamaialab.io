---
title: quando o livro abre sozinho em certas páginas
date: 2020-06-02 10:29:57
---

esses livros aos quais a gente volta sempre, e procura sempre a mesma página, o mesmo capítulo, o mesmo artigo. e o livro aprende, abre sozinho muito educamente e prontamente, feito esses gatos que percebem quando você está triste e deitam no seu peito ronronando bem alto.
