---
title: " Bellini e minhas birras"
date: 2010-06-22 20:34:19
---

eu tinha qualquer birra com o Tony Bellotto mesmo sem ter lido nada. achava que essa história de músico se meter a escrever policial não podia dar muito certo. vá vendo que psicanalista se metendo a escrever policial também não tinha sido a mais brilhante das ideias. mas aí uma amiga me emprestou o primeiro policial dele, _Bellini e a esfinge_, dizendo que era bom. ficou na pilha dos emprestados ali dando sopa, junto de uns do Garcia-Roza e outros dois do Camilleri. mas eu estava certa de que começaria a ler e desistiria.

mordi a língua. peguei o livro pra dar uma olhada, pensando em devolver sem ler, mas mudei de ideia. o livro começa bem e aí quis continuar lendo. gostei do narrador, Remo Bellini. ele chega meio resmungão, mas tem a naturalidade que falta a esses protagonistas de policiais brasileiros. Tony Bellotto acertou na escolha de um detetive de agência privada, como mandariam os criadores do gênero. não dá pra negar a influência do Dashiell Hammett e do Raymond Chandler, até mesmo pelas piadinhas, que pelo jeito andam em falta na literatura policial de hoje.

a verdade é que o livro contrariou todas as minhas expectativas negativas: a narrativa não é forçada, o protagonista não é um tipão metido a besta que pega todas as menininhas (tipo Rubem Fonseca se mata) e o autor não teve a pretensão de criar qualquer coisa além de uma boa história policial. pode não ser nenhum primor literário, além de ter umas construções de frase bem esquisitas. sem contar que ele só aprendeu a separar capítulos no segundo livro. mas isso deixa de ser importante quando a história passa atropelando. no pique li o segundo, _Bellini e o demônio_ (gostei mais desse), e agora comecei o terceiro. _Bellini e os espíritos_. já estou achando uma pena que ele não tenha escrito nenhum outro.
