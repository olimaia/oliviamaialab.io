---
title: " bloqueio na estrada"
date: 2011-07-22 11:15:41
---

que era pra estar chegando em la paz, mas ficamos duas horas parados porque um bloqueio, que o povo cava um buraco do tamanho de um carro nessa estrada de terra. entre outras coisas, querem que se asfaltem as estradas. parece que os motoristas todos simpatizam.

mas, mas. bloqueio superado e mais uma hora e meia ao nosso destino. pés congelando. pelo menos a gente trouxe bastante comida e água.
