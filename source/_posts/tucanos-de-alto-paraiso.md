---
title: " tucanos de Alto Paraíso"
date: 2013-12-16 15:26:34
---

todo mundo via os tucanos de Alto Paraíso de Goiás, menos eu. mas aí último dia na cidade antes de tomar o rumo de Brasília e a Elena, uma italiana sensacional que também nos ensinou como se faz a verdadeira pasta italiana, me levou para caminhar no fim do dia pelos lugares em que costumam aparecer os tucanos. andamos pelas bordas da cidade mas nada, nem mesmo olhando ali do alto. perto do Vale Azul ouvimos algumas araras distantes e fomos ameaçadas por uns insetos voadores quando paramos para assistir ao pôr do sol.

seguimos para a rua em que ficam os eucaliptos e estavam lá os turistas ansiosos. chegavam os primeiros papagaios.

eles iam chegando em pares conversando e gritando e fazendo barulho e às vezes iam embora porque certo a vizinhança era muito fofoqueira. num momento pousou do outro lado da rua um gavião com ares de agente secreto e ficou observando insuspeitamente. mas tucanos mesmo... eu estava já um pouco desistida.

fomos descendo a rua dos eucaliptos e de repente quase dei um pulo. "olha um tucano!!" e a Elena com a mão na boca e todo o exagero paspalhão de italiana "shhh!!". ops. por sorte o tucano não se assustou.

já estava quase escuro e eles são muito silenciosos. mas estavam por todos os lados. uns tucanos pequenos escondidos entre os galhos. vez ou outra um deles mudava de árvore e dava para ouvir bem claro o ruído das asas até que pousasse noutro canto. 

a luz não colaborou para as fotos mas finalmente vi os tucanos que todo mundo sempre vê todos os dias. só não vi as naves espaciais e luzes no céu que também todos na cidade costumam ver com certa frequência.
