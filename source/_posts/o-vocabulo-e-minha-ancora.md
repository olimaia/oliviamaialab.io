---
title: " o vocábulo é minha âncora"
date: 2010-11-19 00:35:00
---

> A vida é ruim, eu sei. Mas ainda não vou cortar a teia da própria vida feito ela minha mãe: o vocábulo é minha âncora; aqui desta mesa-mirante observo o anoitecer dos outros para esquecer-me do próprio crepúsculo.

primeiras linhas de [**_Minha mãe se matou sem dizer adeus_**](http://www.bondfaro.com.br/preco--livros--minha-mae-se-matou-sem-dizer-adeus-9788501090935.html?auto=1), do Evandro Affonso Ferreira. mais:

> Estou triste; não porque vou deixar a vida; mas porque nunca estive nela. Tempo todo caminhando temeroso pelo acostamento. Três senhoras decrépitas ali na mesa mais adiante conversam em hebraico; não sabem que sou o verdadeiro estrangeiro neste mundo. Minha mãe desata de súbito pião da fieira fazendo-o girar na calçada; agora o apoia ainda girando sobre a palma da mão. Tenho cinco seis anos se tanto. Fico perplexo-deslumbrado com tão encantador malabarismo. Mãe-moleque. Veio menina por descuido da natureza; viveu menino tempo todo. Encontrei analogia definitiva: minha vida foi pião sem fieira jogado num canto qualquer da gaveta; brinquedo inútil. O mundo continuará girando sem mim; sem minha mãe; sem Hipócrates; sem Demócrito.

poderia continuar. dá pra abrir em qualquer página e encontrar pedaço pra ser citado. o livro todo é bom demais. pra ler em voz alta, recitando devagar. eu, que tantas vezes ouvi o próprio Evandro lendo pra nós uns pedaços quando o encontrava em sua _mesa-mirante_ lá no shopping Higienópolis, não posso deixar de ouvi-lo quando leio, e a leitura segue adiante deslizando. esse é o primeiro de outros que virão. depois de um tempo sem lançar livro nenhum, o Evandro volta melhor e mais terrível. recomendo muito.
