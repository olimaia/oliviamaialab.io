---
title: "bloom's day"
date: "2019-06-16 15:11"
---

> Há escritores que moram em personagens como há putas que moram em esquinas. James Joyce era um homem que morava em Bloom.
>
> De resto havia um amigo de todos que era o homem mais lento do mundo: demorava mais de seissentas páginas a percorrer um dia.
>
> Homem meio inteligente meio parvo, mas que só actuava com metade de si.
>
> <footer>Gonçalo M. Tavares</footer>
>
> <footer><cite>Biblioteca</cite></footer>
