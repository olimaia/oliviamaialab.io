---
title: " o indizível"
date: 2016-09-18 11:29:11
---

> Não sei se não tenho nada a dizer, sei que não digo nada; não sei se o que teria a dizer não é dito por ser indizível (o indizível não está escondido na escrita, é aquilo que muito antes a desencadeou); sei que o que digo é branco, é neutro, é signo de uma vez por todas de um aniquilamento de uma vez por todas.

- _W ou a memória da infância_, Georges Perec
