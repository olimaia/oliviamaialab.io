---
title: " serra gaúcha"
date: 2012-11-25 11:55:41
---

que esses cânions surgem assim do nada, depois de uma colina ou de uma curva ao redor de um trecho de mata fechada, e súbito acaba o campo num buraco gigantesco de 700 ou 900 metros de profundidade santa catarina adentro. cinco ou dezoito quilômetros num dia e tanto faz porque na verdade eu poderia passar o resto da minha vida caminhando.
