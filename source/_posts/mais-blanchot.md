---
title: " mais Blanchot"
date: 2010-03-08 01:06:49
---

> ... esse poder de representar as coisas pela ausência e de manifestar pelo distanciamento, que está no centro da arte, poder que parece afastar as coisas para dizê-las, mantê-las à distância para que elas se esclareçam, poder de transformação, de tradução, em que é esse próprio afastamento (o espaço) que transforma e traduz, que torna visíveis as coisas invisíveis, transparentes as coisas visíveis, torna-se assim visível nelas e se descobre então como o fundo luminoso de invisibilidade e de irrealidade de onde tudo vem e onde tudo se acaba.

Maurice Blanchot _O livro por vir_
