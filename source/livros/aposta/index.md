---
title: aposta
layout: livro
photos:
  - /img/capas_livros/capa-aposta.jpg
páginas: 40
editora: Edição da autora
formato: 11,5 x 14
data: mar/2019
goodreads: https://www.goodreads.com/book/show/44638517-aposta
id_compra_digital: APOSTA - ebook
preco_digital: 'R$ 3.00'
preco_digital_original: 'R$ 5.00'
---

## sinopse

Téo só queria alguns dias tranquilos visitando a família em Buenos Aires. Em vez disso, uma investigação inesperada: um amigo do primo, dos tempos de escola, desapareceu ao viajar às serras de Córboda para uma entrevista de emprego.

Mas qual pode ser a relação de um engenheiro pacato da capital um dono de cassinos no interior do país? No frio gelado do inverno cordobês, Téo se vê enroscado numa trama nebulosa que envolve crimes passados e pedaços de ossos humanos, e precisa agir rápido para evitar que mais uma tragédia silenciosa passe despercebida na região.

## comprar

Você pode comprar o e-book sem DRM aqui, diretamente comigo.
