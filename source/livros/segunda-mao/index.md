---
title: segunda mão
date: 2019-04-25 18:41:32
layout: livro
photos:
  - /img/capas_livros/segmao.png
páginas: 216
editora: edição da autora
isbn: 978-85-907771-1-3
formato: 11,5 x 17,5
peso: 188g
data: out/2010
infoextra: A edição impressa foi publicada com patrocínio do ProAC de 2010, da Secretaria da Cultura de São Paulo.
goodreads: https://www.goodreads.com/book/show/17457282-segunda-m-o
id_compra_impresso: SEGUNDA MAO - impresso
preco_impresso: 'R$ 19.90'
---

## sinopse

Depois de quase dez anos de certezas dentro da polícia, o investigador Pedro Rodriguez teve um começo de ano tumultuado: um final complicado de relacionamento e um acidente que lhe tirou parte da audição e quase lhe tirou a vida. Para piorar as coisas, é incumbido de investigação envolvendo o suposto suicídio de um primo de seu delegado, em uma trama que envolve lavagem de dinheiro, mentiras, corrupção e a cúpula da polícia.

## comprar

Você pode comprar seu exemplar assinado aqui, diretamente comigo. Se preferir, pode também adquirir o e-book na [Livraria Cultura](http://www.livrariacultura.com.br/p/segunda-mao-106082934), [loja Kobo](https://store.kobobooks.com/pt-br/ebook/segunda-mao) ou [Amazon](https://www.amazon.com.br/Segunda-mo-Olivia-Maia-ebook/dp/B01M28ML00/).
