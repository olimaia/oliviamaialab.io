---
title: epidemia
layout: livro
photos:
  - /img/capas_livros/capa-epidemia.jpg
páginas: 40
editora: Edição da autora
formato: 11,5 x 14
data: mar/2019
goodreads: https://www.goodreads.com/book/show/44638513-epidemia
id_compra_digital: APOSTA - ebook
preco_digital: 'R$ 3.00'
preco_digital_original: 'R$ 5.00'
---

## sinopse

Dores no joelho, na cabeça, nas costas. As dores estão entre o que temos de mais privado e incomunicável. Mas e se pudéssemos sentir a dor alheia como se no nosso próprio corpo? Ao final de um dia cansativo na trilha, em uma cidadezinha turística, um grupo de amigos parece adquirir essa habilidade esquisita. Teria sido uma brincadeira — mas nos dias que seguem, sem explicação, o surto empático atinge a cidade inteira: as crianças, as senhoras na praça, os médicos e dentistas.

## comprar

Você pode comprar o e-book sem DRM aqui, diretamente comigo. 
