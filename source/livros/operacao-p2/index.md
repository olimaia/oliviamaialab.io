---
title: operação p-2
layout: livro
photos:
  - /img/capas_livros/op2.png
páginas: 164
editora: Edição da autora
isbn: 978-85-907771-2-0
formato: 11,5 x 17,5
peso: 138g
data: dez/2007
goodreads: https://www.goodreads.com/book/show/33127398-opera-o-p-2
id_compra_impresso: OPERACAO P-2 - impresso
preco_impresso: 'R$ 19.90'

---

## sinopse

Para Leonardo, o passado é um roteiro na memória e uma pilha de provas de História a corrigir. Mas isso está prestes a mudar – e em velocidade vertiginosa. Um jornalista e professor universitário é assassinado ao pesquisar desvios de verbas no período da ditadura militar; queima de arquivo? Como herança maldita para Rafael, seu aluno (e aprendiz de Philip Marlowe), três nomes e uma instrução: “procura o investigador Mateus, no DEIC”. As descobertas da polícia despertam na mídia a curiosidade por uma suposta organização de guerrilha que funcionaria até hoje. Operação P-2? Outras mortes sucedem e Leonardo é pressionado em direção ao inevitável: um confronto com as lembranças de uma identidade que já não pensava ter, há quase vinte anos.

## comprar

Você pode comprar seu exemplar assinado nesta página, diretamente comigo. Se preferir, pode também adquirir o e-book na [Livraria Cultura](http://www.livrariacultura.com.br/p/operacao-p-2-106082977), loja [Kobo](https://store.kobobooks.com/pt-br/ebook/operacao-p-2) ou [Amazon](https://www.amazon.com.br/Operao-P-2-Olivia-Maia-ebook/dp/B01M31B6XY/).
