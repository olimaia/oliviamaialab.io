---
title: Sherlock Holmes – O jogo continua
layout: livro
photos:
  - /img/capas_livros/sherlock.jpg
páginas: 201
editora: Editora Draco
data: mai/2017
---

## sinopse

Em *Sherlock Holmes – O jogo continua*, o mestre da dedução está pronto para desvendar novos casos, e mais uma vez imaginado por um seleto time de autores brasileiros. Com organização de Marcelo A. Galvão e Cirilo S. Lemos, participam dessa homenagem ao inquieto detetive da rua Baker 221B os cronistas A. Z. Cordenonsi, Antonio Luiz M. C. Costa, Flávio Medeiros Jr., Leandro Leme, Marcelo Jacinto Ribeiro, Olivia Maia, Ricardo de Brito e Roberta Grassi.

Elementar, meu caro leitor. Obra máxima de Sir Arthur Conan Doyle, Sherlock Holmes é um dos mais duradouros ícones culturais de todos os tempos. Reinventado no cinema, TV, quadrinhos, games e sempre celebrado na literatura, o detetive continua a encantar gerações com seu delicado equilíbrio entre racionalidade pura, personalidade única e energia explosiva.

## comprar

- [amazon](https://www.amazon.com.br/Sherlock-Holmes-continua-detetive-Portuguese-ebook/dp/B071J7N7FK)
