title: " A casa no morro\t\t"
url: 57.html
id: 57
categories:
  - artigos &amp; contos
date: 2022-02-02 14:19:06
tags:
---

Conto policial em cinco partes publicado em formato de folhetim na seção "Palavra" do _Le Monde diplomatique Brasil Online_, ao longo de maio de 2008.  
  
\[Leia:  
   [parte 1](http://diplo.org.br/2008-05,a2377);  
   [parte 2](http://diplo.org.br/2008-05,a2385);  
   [parte 3](http://diplo.org.br/2008-05,a2398);  
   [parte 4](http://diplo.org.br/2008-05,a2414);  
   [final](http://diplo.org.br/2008-05,a2428).\]